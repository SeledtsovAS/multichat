#include <string>
#include <algorithm>
#include <QString>
#include <QSharedPointer>
#include <vector>

#include "textprotocolimpl.h"

#include "messages/basemessage.h"
#include "messages/message.h"
#include "messages/messageaccess.h"
#include "messages/messageansweronline.h"
#include "messages/messagefile.h"
#include "messages/messagegetaccess.h"
#include "messages/messageonline.h"

TextProtocolImpl::~TextProtocolImpl() {

}

std::vector<std::string> TextProtocolImpl::split(std::string const& message)
{
    std::vector<std::string> result { };
    auto from = message.begin();
    auto to = from;

    if (message.size() <= 3)
        return result;

    auto i = from;
    while (i != message.end() - 2) {
        if (*i == '|' && *(i + 1) == '*' && *(i + 2) == '|') {
            to = i;
            result.push_back(std::string(from, to));
            from = to + 3;
        }

        std::advance(i, 1);
    }
    return result;
}

std::string TextProtocolImpl::toString(BaseMessage const& message)
{
    std::string result = QString::number(static_cast<int>(message.getType())).toStdString() + "|*|";
    result += QString::number(message.getFrom()).toStdString() +
            "|*|" + QString::number(message.getTo()).toStdString() + "|*|";

    if (message.getType() == BaseMessage::TypeMessage::SMS) {
        result += message.getKey() + "|*|";
        result += message.getBody() + "|*|";
    }
    else if (message.getType() == BaseMessage::TypeMessage::FILE) {
        result += message.getKey() + "|*|";
        result += message.getBody() + "|*|";
        result += message.getTitle() + "|*|";
    }
    else if (message.getType() == BaseMessage::TypeMessage::LIST_ONLINE) {
        result += message.getBody() + "|*|";
    }
    else if (message.getType() == BaseMessage::TypeMessage::GET_ACCESS) {
        result += message.getTitle() + "|*|";
    }
    else if (message.getType() == BaseMessage::TypeMessage::ACCESS) {
        result += message.getTitle() + "|*|";
        result += message.getBody() + "|*|";
    }

    return result;
}

QSharedPointer<BaseMessage> TextProtocolImpl::fromString(std::string const& message)
{
    QSharedPointer<BaseMessage> ptrResult { nullptr };
    std::vector<std::string> parseMessage = split(message);
    BaseMessage::TypeMessage type = static_cast<BaseMessage::TypeMessage>(QString(parseMessage[0].c_str()).toInt());
    int from = QString(parseMessage[1].c_str()).toInt();
    int to = QString(parseMessage[2].c_str()).toInt();

    if (type == BaseMessage::TypeMessage::GET_LIST_ONLINE) {
        ptrResult = QSharedPointer<BaseMessage>(new MessageAnswerOnline(from, to));
    }
    else if (type == BaseMessage::TypeMessage::LIST_ONLINE) {
        ptrResult = QSharedPointer<BaseMessage>(new MessageOnline(from, to));
        ptrResult->setBody(parseMessage[3]);
    }
    else if (type == BaseMessage::TypeMessage::SMS) {
        ptrResult = QSharedPointer<BaseMessage>(new Message(from, to));
        ptrResult->setKey(parseMessage[3]);
        ptrResult->setBody(parseMessage[4]);
    }
    else if (type == BaseMessage::TypeMessage::FILE) {
        ptrResult = QSharedPointer<BaseMessage>(new MessageFile(from, to, parseMessage[5]));
        ptrResult->setKey(parseMessage[3]);
        ptrResult->setBody(parseMessage[4]);
    }
    else if (type == BaseMessage::TypeMessage::GET_ACCESS) {
        ptrResult = QSharedPointer<BaseMessage>(new MessageGetAccess(from, to,
                                     QString(parseMessage[3].c_str()).toInt()));
    }
    else if (type == BaseMessage::TypeMessage::ACCESS) {
        ptrResult = QSharedPointer<BaseMessage>(new MessageAccess(from, to, parseMessage[4]));
        ptrResult->setTitle(parseMessage[3]);
    }

    return ptrResult;
}
