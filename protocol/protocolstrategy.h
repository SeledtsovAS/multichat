#ifndef PROTOCOLSTRATEGY_H
#define PROTOCOLSTRATEGY_H

#include <QSharedPointer>

class BaseMessage;
class ProtocolImpl;

class ProtocolStrategy
{
public:
    ProtocolStrategy(ProtocolImpl* impl);
    virtual ~ProtocolStrategy();

    std::string toString(BaseMessage const& message);
    QSharedPointer<BaseMessage> fromString(std::string const& message);

private:
    QSharedPointer<ProtocolImpl> m_protocol;
};

#endif // PROTOCOLSTRATEGY_H
