#ifndef BUFFERPROTOCOLIMPL_H
#define BUFFERPROTOCOLIMPL_H

#include <string>

#include "protocolimpl.h"

template <typename T>
class QSharedPointer;

class BaseMessage;

class BufferProtocolImpl: public ProtocolImpl
{
public:
    ~BufferProtocolImpl();

    std::string toString(BaseMessage const& message);
    QSharedPointer<BaseMessage> fromString(std::string const& message);
};

#endif // BUFFERPROTOCOLIMPL_H
