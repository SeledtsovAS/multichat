// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: protocol.proto

#ifndef PROTOBUF_protocol_2eproto__INCLUDED
#define PROTOBUF_protocol_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 2005000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 2005000 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/message.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/generated_enum_reflection.h>
#include <google/protobuf/unknown_field_set.h>
// @@protoc_insertion_point(includes)

namespace protocol {

// Internal implementation detail -- do not call these.
void  protobuf_AddDesc_protocol_2eproto();
void protobuf_AssignDesc_protocol_2eproto();
void protobuf_ShutdownFile_protocol_2eproto();

class Message;

enum Message_TYPE_MESSAGE {
  Message_TYPE_MESSAGE_SMS = 0,
  Message_TYPE_MESSAGE_FILE = 1,
  Message_TYPE_MESSAGE_LIST_CONSUMER = 2,
  Message_TYPE_MESSAGE_GET_LIST = 3,
  Message_TYPE_MESSAGE_ACCESS = 4,
  Message_TYPE_MESSAGE_GET_ACCESS = 5
};
bool Message_TYPE_MESSAGE_IsValid(int value);
const Message_TYPE_MESSAGE Message_TYPE_MESSAGE_TYPE_MESSAGE_MIN = Message_TYPE_MESSAGE_SMS;
const Message_TYPE_MESSAGE Message_TYPE_MESSAGE_TYPE_MESSAGE_MAX = Message_TYPE_MESSAGE_GET_ACCESS;
const int Message_TYPE_MESSAGE_TYPE_MESSAGE_ARRAYSIZE = Message_TYPE_MESSAGE_TYPE_MESSAGE_MAX + 1;

const ::google::protobuf::EnumDescriptor* Message_TYPE_MESSAGE_descriptor();
inline const ::std::string& Message_TYPE_MESSAGE_Name(Message_TYPE_MESSAGE value) {
  return ::google::protobuf::internal::NameOfEnum(
    Message_TYPE_MESSAGE_descriptor(), value);
}
inline bool Message_TYPE_MESSAGE_Parse(
    const ::std::string& name, Message_TYPE_MESSAGE* value) {
  return ::google::protobuf::internal::ParseNamedEnum<Message_TYPE_MESSAGE>(
    Message_TYPE_MESSAGE_descriptor(), name, value);
}
// ===================================================================

class Message : public ::google::protobuf::Message {
 public:
  Message();
  virtual ~Message();

  Message(const Message& from);

  inline Message& operator=(const Message& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _unknown_fields_;
  }

  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return &_unknown_fields_;
  }

  static const ::google::protobuf::Descriptor* descriptor();
  static const Message& default_instance();

  void Swap(Message* other);

  // implements Message ----------------------------------------------

  Message* New() const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const Message& from);
  void MergeFrom(const Message& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  public:

  ::google::protobuf::Metadata GetMetadata() const;

  // nested types ----------------------------------------------------

  typedef Message_TYPE_MESSAGE TYPE_MESSAGE;
  static const TYPE_MESSAGE SMS = Message_TYPE_MESSAGE_SMS;
  static const TYPE_MESSAGE FILE = Message_TYPE_MESSAGE_FILE;
  static const TYPE_MESSAGE LIST_CONSUMER = Message_TYPE_MESSAGE_LIST_CONSUMER;
  static const TYPE_MESSAGE GET_LIST = Message_TYPE_MESSAGE_GET_LIST;
  static const TYPE_MESSAGE ACCESS = Message_TYPE_MESSAGE_ACCESS;
  static const TYPE_MESSAGE GET_ACCESS = Message_TYPE_MESSAGE_GET_ACCESS;
  static inline bool TYPE_MESSAGE_IsValid(int value) {
    return Message_TYPE_MESSAGE_IsValid(value);
  }
  static const TYPE_MESSAGE TYPE_MESSAGE_MIN =
    Message_TYPE_MESSAGE_TYPE_MESSAGE_MIN;
  static const TYPE_MESSAGE TYPE_MESSAGE_MAX =
    Message_TYPE_MESSAGE_TYPE_MESSAGE_MAX;
  static const int TYPE_MESSAGE_ARRAYSIZE =
    Message_TYPE_MESSAGE_TYPE_MESSAGE_ARRAYSIZE;
  static inline const ::google::protobuf::EnumDescriptor*
  TYPE_MESSAGE_descriptor() {
    return Message_TYPE_MESSAGE_descriptor();
  }
  static inline const ::std::string& TYPE_MESSAGE_Name(TYPE_MESSAGE value) {
    return Message_TYPE_MESSAGE_Name(value);
  }
  static inline bool TYPE_MESSAGE_Parse(const ::std::string& name,
      TYPE_MESSAGE* value) {
    return Message_TYPE_MESSAGE_Parse(name, value);
  }

  // accessors -------------------------------------------------------

  // required int32 src_id = 1;
  inline bool has_src_id() const;
  inline void clear_src_id();
  static const int kSrcIdFieldNumber = 1;
  inline ::google::protobuf::int32 src_id() const;
  inline void set_src_id(::google::protobuf::int32 value);

  // required int32 dest_id = 2;
  inline bool has_dest_id() const;
  inline void clear_dest_id();
  static const int kDestIdFieldNumber = 2;
  inline ::google::protobuf::int32 dest_id() const;
  inline void set_dest_id(::google::protobuf::int32 value);

  // optional bytes key = 7;
  inline bool has_key() const;
  inline void clear_key();
  static const int kKeyFieldNumber = 7;
  inline const ::std::string& key() const;
  inline void set_key(const ::std::string& value);
  inline void set_key(const char* value);
  inline void set_key(const void* value, size_t size);
  inline ::std::string* mutable_key();
  inline ::std::string* release_key();
  inline void set_allocated_key(::std::string* key);

  // required .protocol.Message.TYPE_MESSAGE type = 3;
  inline bool has_type() const;
  inline void clear_type();
  static const int kTypeFieldNumber = 3;
  inline ::protocol::Message_TYPE_MESSAGE type() const;
  inline void set_type(::protocol::Message_TYPE_MESSAGE value);

  // optional bytes body = 4;
  inline bool has_body() const;
  inline void clear_body();
  static const int kBodyFieldNumber = 4;
  inline const ::std::string& body() const;
  inline void set_body(const ::std::string& value);
  inline void set_body(const char* value);
  inline void set_body(const void* value, size_t size);
  inline ::std::string* mutable_body();
  inline ::std::string* release_body();
  inline void set_allocated_body(::std::string* body);

  // optional string file_name = 5;
  inline bool has_file_name() const;
  inline void clear_file_name();
  static const int kFileNameFieldNumber = 5;
  inline const ::std::string& file_name() const;
  inline void set_file_name(const ::std::string& value);
  inline void set_file_name(const char* value);
  inline void set_file_name(const char* value, size_t size);
  inline ::std::string* mutable_file_name();
  inline ::std::string* release_file_name();
  inline void set_allocated_file_name(::std::string* file_name);

  // repeated int32 id_consumer = 6;
  inline int id_consumer_size() const;
  inline void clear_id_consumer();
  static const int kIdConsumerFieldNumber = 6;
  inline ::google::protobuf::int32 id_consumer(int index) const;
  inline void set_id_consumer(int index, ::google::protobuf::int32 value);
  inline void add_id_consumer(::google::protobuf::int32 value);
  inline const ::google::protobuf::RepeatedField< ::google::protobuf::int32 >&
      id_consumer() const;
  inline ::google::protobuf::RepeatedField< ::google::protobuf::int32 >*
      mutable_id_consumer();

  // @@protoc_insertion_point(class_scope:protocol.Message)
 private:
  inline void set_has_src_id();
  inline void clear_has_src_id();
  inline void set_has_dest_id();
  inline void clear_has_dest_id();
  inline void set_has_key();
  inline void clear_has_key();
  inline void set_has_type();
  inline void clear_has_type();
  inline void set_has_body();
  inline void clear_has_body();
  inline void set_has_file_name();
  inline void clear_has_file_name();

  ::google::protobuf::UnknownFieldSet _unknown_fields_;

  ::google::protobuf::int32 src_id_;
  ::google::protobuf::int32 dest_id_;
  ::std::string* key_;
  ::std::string* body_;
  ::std::string* file_name_;
  ::google::protobuf::RepeatedField< ::google::protobuf::int32 > id_consumer_;
  int type_;

  mutable int _cached_size_;
  ::google::protobuf::uint32 _has_bits_[(7 + 31) / 32];

  friend void  protobuf_AddDesc_protocol_2eproto();
  friend void protobuf_AssignDesc_protocol_2eproto();
  friend void protobuf_ShutdownFile_protocol_2eproto();

  void InitAsDefaultInstance();
  static Message* default_instance_;
};
// ===================================================================


// ===================================================================

// Message

// required int32 src_id = 1;
inline bool Message::has_src_id() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void Message::set_has_src_id() {
  _has_bits_[0] |= 0x00000001u;
}
inline void Message::clear_has_src_id() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void Message::clear_src_id() {
  src_id_ = 0;
  clear_has_src_id();
}
inline ::google::protobuf::int32 Message::src_id() const {
  return src_id_;
}
inline void Message::set_src_id(::google::protobuf::int32 value) {
  set_has_src_id();
  src_id_ = value;
}

// required int32 dest_id = 2;
inline bool Message::has_dest_id() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
inline void Message::set_has_dest_id() {
  _has_bits_[0] |= 0x00000002u;
}
inline void Message::clear_has_dest_id() {
  _has_bits_[0] &= ~0x00000002u;
}
inline void Message::clear_dest_id() {
  dest_id_ = 0;
  clear_has_dest_id();
}
inline ::google::protobuf::int32 Message::dest_id() const {
  return dest_id_;
}
inline void Message::set_dest_id(::google::protobuf::int32 value) {
  set_has_dest_id();
  dest_id_ = value;
}

// optional bytes key = 7;
inline bool Message::has_key() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
inline void Message::set_has_key() {
  _has_bits_[0] |= 0x00000004u;
}
inline void Message::clear_has_key() {
  _has_bits_[0] &= ~0x00000004u;
}
inline void Message::clear_key() {
  if (key_ != &::google::protobuf::internal::kEmptyString) {
    key_->clear();
  }
  clear_has_key();
}
inline const ::std::string& Message::key() const {
  return *key_;
}
inline void Message::set_key(const ::std::string& value) {
  set_has_key();
  if (key_ == &::google::protobuf::internal::kEmptyString) {
    key_ = new ::std::string;
  }
  key_->assign(value);
}
inline void Message::set_key(const char* value) {
  set_has_key();
  if (key_ == &::google::protobuf::internal::kEmptyString) {
    key_ = new ::std::string;
  }
  key_->assign(value);
}
inline void Message::set_key(const void* value, size_t size) {
  set_has_key();
  if (key_ == &::google::protobuf::internal::kEmptyString) {
    key_ = new ::std::string;
  }
  key_->assign(reinterpret_cast<const char*>(value), size);
}
inline ::std::string* Message::mutable_key() {
  set_has_key();
  if (key_ == &::google::protobuf::internal::kEmptyString) {
    key_ = new ::std::string;
  }
  return key_;
}
inline ::std::string* Message::release_key() {
  clear_has_key();
  if (key_ == &::google::protobuf::internal::kEmptyString) {
    return NULL;
  } else {
    ::std::string* temp = key_;
    key_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
    return temp;
  }
}
inline void Message::set_allocated_key(::std::string* key) {
  if (key_ != &::google::protobuf::internal::kEmptyString) {
    delete key_;
  }
  if (key) {
    set_has_key();
    key_ = key;
  } else {
    clear_has_key();
    key_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
  }
}

// required .protocol.Message.TYPE_MESSAGE type = 3;
inline bool Message::has_type() const {
  return (_has_bits_[0] & 0x00000008u) != 0;
}
inline void Message::set_has_type() {
  _has_bits_[0] |= 0x00000008u;
}
inline void Message::clear_has_type() {
  _has_bits_[0] &= ~0x00000008u;
}
inline void Message::clear_type() {
  type_ = 0;
  clear_has_type();
}
inline ::protocol::Message_TYPE_MESSAGE Message::type() const {
  return static_cast< ::protocol::Message_TYPE_MESSAGE >(type_);
}
inline void Message::set_type(::protocol::Message_TYPE_MESSAGE value) {
  assert(::protocol::Message_TYPE_MESSAGE_IsValid(value));
  set_has_type();
  type_ = value;
}

// optional bytes body = 4;
inline bool Message::has_body() const {
  return (_has_bits_[0] & 0x00000010u) != 0;
}
inline void Message::set_has_body() {
  _has_bits_[0] |= 0x00000010u;
}
inline void Message::clear_has_body() {
  _has_bits_[0] &= ~0x00000010u;
}
inline void Message::clear_body() {
  if (body_ != &::google::protobuf::internal::kEmptyString) {
    body_->clear();
  }
  clear_has_body();
}
inline const ::std::string& Message::body() const {
  return *body_;
}
inline void Message::set_body(const ::std::string& value) {
  set_has_body();
  if (body_ == &::google::protobuf::internal::kEmptyString) {
    body_ = new ::std::string;
  }
  body_->assign(value);
}
inline void Message::set_body(const char* value) {
  set_has_body();
  if (body_ == &::google::protobuf::internal::kEmptyString) {
    body_ = new ::std::string;
  }
  body_->assign(value);
}
inline void Message::set_body(const void* value, size_t size) {
  set_has_body();
  if (body_ == &::google::protobuf::internal::kEmptyString) {
    body_ = new ::std::string;
  }
  body_->assign(reinterpret_cast<const char*>(value), size);
}
inline ::std::string* Message::mutable_body() {
  set_has_body();
  if (body_ == &::google::protobuf::internal::kEmptyString) {
    body_ = new ::std::string;
  }
  return body_;
}
inline ::std::string* Message::release_body() {
  clear_has_body();
  if (body_ == &::google::protobuf::internal::kEmptyString) {
    return NULL;
  } else {
    ::std::string* temp = body_;
    body_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
    return temp;
  }
}
inline void Message::set_allocated_body(::std::string* body) {
  if (body_ != &::google::protobuf::internal::kEmptyString) {
    delete body_;
  }
  if (body) {
    set_has_body();
    body_ = body;
  } else {
    clear_has_body();
    body_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
  }
}

// optional string file_name = 5;
inline bool Message::has_file_name() const {
  return (_has_bits_[0] & 0x00000020u) != 0;
}
inline void Message::set_has_file_name() {
  _has_bits_[0] |= 0x00000020u;
}
inline void Message::clear_has_file_name() {
  _has_bits_[0] &= ~0x00000020u;
}
inline void Message::clear_file_name() {
  if (file_name_ != &::google::protobuf::internal::kEmptyString) {
    file_name_->clear();
  }
  clear_has_file_name();
}
inline const ::std::string& Message::file_name() const {
  return *file_name_;
}
inline void Message::set_file_name(const ::std::string& value) {
  set_has_file_name();
  if (file_name_ == &::google::protobuf::internal::kEmptyString) {
    file_name_ = new ::std::string;
  }
  file_name_->assign(value);
}
inline void Message::set_file_name(const char* value) {
  set_has_file_name();
  if (file_name_ == &::google::protobuf::internal::kEmptyString) {
    file_name_ = new ::std::string;
  }
  file_name_->assign(value);
}
inline void Message::set_file_name(const char* value, size_t size) {
  set_has_file_name();
  if (file_name_ == &::google::protobuf::internal::kEmptyString) {
    file_name_ = new ::std::string;
  }
  file_name_->assign(reinterpret_cast<const char*>(value), size);
}
inline ::std::string* Message::mutable_file_name() {
  set_has_file_name();
  if (file_name_ == &::google::protobuf::internal::kEmptyString) {
    file_name_ = new ::std::string;
  }
  return file_name_;
}
inline ::std::string* Message::release_file_name() {
  clear_has_file_name();
  if (file_name_ == &::google::protobuf::internal::kEmptyString) {
    return NULL;
  } else {
    ::std::string* temp = file_name_;
    file_name_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
    return temp;
  }
}
inline void Message::set_allocated_file_name(::std::string* file_name) {
  if (file_name_ != &::google::protobuf::internal::kEmptyString) {
    delete file_name_;
  }
  if (file_name) {
    set_has_file_name();
    file_name_ = file_name;
  } else {
    clear_has_file_name();
    file_name_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
  }
}

// repeated int32 id_consumer = 6;
inline int Message::id_consumer_size() const {
  return id_consumer_.size();
}
inline void Message::clear_id_consumer() {
  id_consumer_.Clear();
}
inline ::google::protobuf::int32 Message::id_consumer(int index) const {
  return id_consumer_.Get(index);
}
inline void Message::set_id_consumer(int index, ::google::protobuf::int32 value) {
  id_consumer_.Set(index, value);
}
inline void Message::add_id_consumer(::google::protobuf::int32 value) {
  id_consumer_.Add(value);
}
inline const ::google::protobuf::RepeatedField< ::google::protobuf::int32 >&
Message::id_consumer() const {
  return id_consumer_;
}
inline ::google::protobuf::RepeatedField< ::google::protobuf::int32 >*
Message::mutable_id_consumer() {
  return &id_consumer_;
}


// @@protoc_insertion_point(namespace_scope)

}  // namespace protocol

#ifndef SWIG
namespace google {
namespace protobuf {

template <>
inline const EnumDescriptor* GetEnumDescriptor< ::protocol::Message_TYPE_MESSAGE>() {
  return ::protocol::Message_TYPE_MESSAGE_descriptor();
}

}  // namespace google
}  // namespace protobuf
#endif  // SWIG

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_protocol_2eproto__INCLUDED
