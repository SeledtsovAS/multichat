#ifndef PROTOCOLIMPL_H
#define PROTOCOLIMPL_H

#include <string>

#include "messages/basemessage.h"

template <typename T>
class QSharedPointer;

class BaseMessage;

class ProtocolImpl
{
public:
    virtual ~ProtocolImpl();

    virtual std::string toString(BaseMessage const& message) = 0;
    virtual QSharedPointer<BaseMessage> fromString(std::string const& message) = 0;
};

#endif // PROTOCOLIMPL_H
