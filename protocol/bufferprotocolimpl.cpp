#include <QSharedPointer>
#include <QString>
#include <QStringList>

#include "messages/basemessage.h"
#include "messages/message.h"
#include "messages/messageonline.h"
#include "messages/messageansweronline.h"
#include "messages/messageaccess.h"
#include "messages/messagegetaccess.h"
#include "messages/messagefile.h"

#include "protocol.pb.h"
#include "bufferprotocolimpl.h"

BufferProtocolImpl::~BufferProtocolImpl() {

}

std::string BufferProtocolImpl::toString(BaseMessage const& message)
{
    protocol::Message result;

    result.set_src_id(message.getFrom());
    result.set_dest_id(message.getTo());

    if (message.getType() == BaseMessage::TypeMessage::SMS) {
        result.set_type(protocol::Message::TYPE_MESSAGE::Message_TYPE_MESSAGE_SMS);
        result.set_body(message.getBody());
        result.set_key(message.getKey());
    }
    else if (message.getType() == BaseMessage::TypeMessage::FILE) {
        result.set_type(protocol::Message::TYPE_MESSAGE::Message_TYPE_MESSAGE_FILE);
        result.set_body(message.getBody());
        result.set_file_name(message.getTitle());
        result.set_key(message.getKey());
    }
    else if (message.getType() == BaseMessage::TypeMessage::LIST_ONLINE) {
        result.set_type(protocol::Message::TYPE_MESSAGE::Message_TYPE_MESSAGE_LIST_CONSUMER);

        QStringList listId =  QString(message.getBody().c_str()).split(" ");
        for (int i = 0; i < listId.size(); i++)
            result.add_id_consumer(listId[i].toInt());
    }
    else if (message.getType() == BaseMessage::TypeMessage::GET_LIST_ONLINE) {
        result.set_type(protocol::Message::TYPE_MESSAGE::Message_TYPE_MESSAGE_GET_LIST);
    }
    else if (message.getType() == BaseMessage::TypeMessage::GET_ACCESS) {
        result.set_type(protocol::Message::TYPE_MESSAGE::Message_TYPE_MESSAGE_GET_ACCESS);
        result.set_file_name(message.getTitle());
    }
    else if (message.getType() == BaseMessage::TypeMessage::ACCESS) {
        result.set_type(protocol::Message::TYPE_MESSAGE::Message_TYPE_MESSAGE_ACCESS);
        result.set_file_name(message.getTitle());
        result.set_body(message.getBody());
    }

    std::string serializedObject = "";
    result.SerializeToString(&serializedObject);

    return serializedObject;
}

QSharedPointer<BaseMessage> BufferProtocolImpl::fromString(std::string const& message)
{
    protocol::Message result;
    result.ParseFromString(message);
    BaseMessage* ptrMessage = nullptr;

    if (result.type() == protocol::Message::TYPE_MESSAGE::Message_TYPE_MESSAGE_SMS) {
        ptrMessage = new Message(result.src_id(), result.dest_id());
        ptrMessage->setBody(result.body());
        ptrMessage->setKey(result.key());
    }
    else if (result.type() == protocol::Message::TYPE_MESSAGE::Message_TYPE_MESSAGE_FILE) {
        ptrMessage = new MessageFile(result.src_id(), result.dest_id(), result.file_name());
        ptrMessage->setBody(result.body());
        ptrMessage->setKey(result.key());
    }
    else if (result.type() == protocol::Message::TYPE_MESSAGE::Message_TYPE_MESSAGE_LIST_CONSUMER) {
        std::vector<int> vec;
        for (int i = 0; i < result.id_consumer_size(); i++)
            vec.push_back(result.id_consumer(i));
        ptrMessage = new MessageOnline(result.src_id(), result.dest_id(), vec);
    }
    else if (result.type() == protocol::Message::TYPE_MESSAGE::Message_TYPE_MESSAGE_GET_LIST) {
        ptrMessage = new MessageAnswerOnline(result.src_id(), result.dest_id());
    }
    else if (result.type() == protocol::Message::TYPE_MESSAGE::Message_TYPE_MESSAGE_GET_ACCESS) {
        ptrMessage = new MessageGetAccess(result.src_id(), result.dest_id(), QString(result.file_name().c_str()).toInt());
    }
    else if (result.type() == protocol::Message::TYPE_MESSAGE::Message_TYPE_MESSAGE_ACCESS) {
        ptrMessage = new MessageAccess(result.src_id(), result.dest_id(), result.body());
        ptrMessage->setTitle(result.file_name());
    }

    return QSharedPointer<BaseMessage>(ptrMessage);
}
