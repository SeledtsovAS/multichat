#ifndef TEXTPROTOCOLIMPL_H
#define TEXTPROTOCOLIMPL_H

#include <string>
#include <vector>

#include "protocolimpl.h"

template <typename T>
class QSharedPointer;

class BaseMessage;

class BaseMessage;

class TextProtocolImpl: public ProtocolImpl
{
public:
    ~TextProtocolImpl();

    std::string toString(BaseMessage const& message);
    QSharedPointer<BaseMessage> fromString(std::string const& message);

private:
    std::vector<std::string> split(std::string const& message);
};

#endif // TEXTPROTOCOLIMPL_H
