#include <QSharedPointer>
#include <string>

#include "protocolstrategy.h"
#include "protocol/protocolimpl.h"

ProtocolStrategy::ProtocolStrategy(ProtocolImpl* implement):
    m_protocol(QSharedPointer<ProtocolImpl>(implement)) {

}

ProtocolStrategy::~ProtocolStrategy() {

}

std::string ProtocolStrategy::toString(BaseMessage const& message) {
    return m_protocol->toString(message);
}

QSharedPointer<BaseMessage> ProtocolStrategy::fromString(std::string const& message) {
    return m_protocol->fromString(message);
}

