#include <QApplication>

#include "clientchat.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ClientChat chat;
    chat.show();

    return a.exec();
}
