#ifndef IOMANAGER_H
#define IOMANAGER_H

#include <QSharedPointer>

#include "baseiomanager.h"

class BaseMessage;
class ProtocolStrategy;

class IOManager: public BaseIOManager
{
public:
    IOManager(ProtocolStrategy* protocol);
    ~IOManager();

    int recv(QTcpSocket* socket, QSharedPointer<BaseMessage>& message);
    int send(QTcpSocket* socket, BaseMessage& message);

private:
    QSharedPointer<ProtocolStrategy> m_protocol;
};

#endif // IOMANAGER_H
