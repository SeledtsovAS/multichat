#ifndef CRIPTODECORATORIO_H
#define CRIPTODECORATORIO_H

#include <QSharedPointer>

#include "baseiodecorator.h"

class BaseIOManager;
class CryptoPPSystem;
class BaseMessage;

class CriptoDecoratorIO: public BaseIODecorator
{
public:
    CriptoDecoratorIO(BaseIOManager* manager, CryptoPPSystem* crypto);

    int recv(QTcpSocket* socket, QSharedPointer<BaseMessage>& message);
    int send(QTcpSocket* socket, BaseMessage& message);

private:
    QSharedPointer<BaseIOManager> m_ioManager;
    QSharedPointer<CryptoPPSystem> m_crypto;
};

#endif // CRIPTODECORATORIO_H
