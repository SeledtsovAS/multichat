#ifndef BASEIODECORATOR_H
#define BASEIODECORATOR_H

#include <QSharedPointer>

#include "baseiomanager.h"

class BaseMessage;

class BaseIODecorator: public BaseIOManager
{
public:
    virtual ~BaseIODecorator();

    int recv(QTcpSocket* socket, QSharedPointer<BaseMessage>& message) = 0;
    int send(QTcpSocket* socket, BaseMessage& message) = 0;
};

#endif // BASEIODECORATOR_H
