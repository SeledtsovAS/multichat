#ifndef DECORATORFILTER_H
#define DECORATORFILTER_H

#include <QSharedPointer>

#include "baseiodecorator.h"

class BaseIOManager;

class DecoratorFilter: public BaseIODecorator
{
public:
    DecoratorFilter(BaseIOManager* crypto, BaseIOManager* managerIO);

    int recv(QTcpSocket *socket, QSharedPointer<BaseMessage> &message);
    int send(QTcpSocket *socket, BaseMessage &message);

private:
    QSharedPointer<BaseIOManager> m_crypto;
    QSharedPointer<BaseIOManager> m_ioManager;
};

#endif // DECORATORFILTER_H
