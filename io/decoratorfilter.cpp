#include <QSharedPointer>

#include "decoratorfilter.h"
#include "baseiomanager.h"
#include "criptodecoratorio.h"
#include <iostream>

#include "messages/basemessage.h"

DecoratorFilter::DecoratorFilter(BaseIOManager* crypto, BaseIOManager* manager):
    m_crypto(QSharedPointer<BaseIOManager>(crypto)),
    m_ioManager(QSharedPointer<BaseIOManager>(manager)) {

}

int DecoratorFilter::recv(QTcpSocket *socket, QSharedPointer<BaseMessage> &message)
{
    m_crypto->recv(socket, message);
}

int DecoratorFilter::send(QTcpSocket *socket, BaseMessage &message)
{
    if (message.getType() == BaseMessage::TypeMessage::FILE ||
            message.getType() == BaseMessage::TypeMessage::SMS)
        m_crypto->send(socket, message);
    else
        m_ioManager->send(socket, message);
}
