#ifndef BASEIOMANAGER_H
#define BASEIOMANAGER_H

#include <QSharedPointer>

class QTcpSocket;
class BaseMessage;

class BaseIOManager
{
public:
    virtual ~BaseIOManager();

    virtual int recv(QTcpSocket* socket, QSharedPointer<BaseMessage>& message) = 0;
    virtual int send(QTcpSocket* socket, BaseMessage& message) = 0;
};

#endif // BASEIOMANAGER_H
