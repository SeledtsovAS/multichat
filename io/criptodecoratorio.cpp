#include <QTcpSocket>
#include <iostream>
#include <string>

#include "criptodecoratorio.h"
#include "security/cryptoppsystem.h"
#include "messages/basemessage.h"

CriptoDecoratorIO::CriptoDecoratorIO(BaseIOManager* manager, CryptoPPSystem* crypto)
{
    m_ioManager = QSharedPointer<BaseIOManager>(manager);
    m_crypto = QSharedPointer<CryptoPPSystem>(crypto);
}

int CriptoDecoratorIO::recv(QTcpSocket* socket, QSharedPointer<BaseMessage>& message)
{
    int size = m_ioManager->recv(socket, message);

    if (message->getType() != BaseMessage::TypeMessage::FILE &&
            message->getType() != BaseMessage::TypeMessage::SMS)
        return size;

    std::string key(message->getKey());
    std::string body(message->getBody());

    m_crypto->decrypt(key);
    m_crypto->decrypt(key, body);

    message->setBody(body);
    message->setKey(key);

    return size;
}

int CriptoDecoratorIO::send(QTcpSocket* socket, BaseMessage& message)
{
    std::string body = message.getBody();
    std::string key = message.getKey();

    m_crypto->encrypt(key, body);
    m_crypto->encrypt(key);

    message.setBody(body);
    message.setKey(key);

    int size = m_ioManager->send(socket, message);
    return size;
}
