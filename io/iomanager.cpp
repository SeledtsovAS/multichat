#include <QByteArray>
#include <QSharedPointer>
#include <QTcpServer>
#include <QTcpSocket>
#include <QDataStream>
#include <QString>
#include <string>
#include <iostream>

#include "iomanager.h"
#include "messages/basemessage.h"
#include "protocol/protocolstrategy.h"

IOManager::IOManager(ProtocolStrategy *protocol):
    m_protocol(QSharedPointer<ProtocolStrategy>(protocol)) {

}

IOManager::~IOManager() {

}

int IOManager::recv(QTcpSocket* socket, QSharedPointer<BaseMessage>& message)
{
    QDataStream in(socket);
    in.setVersion(QDataStream::Qt_5_2);

    std::string result;
    bool flagReading = true;
    quint16 nextBlockSize = 0;

    while (flagReading)
    {
        if (!nextBlockSize && flagReading)
        {
            if (socket->bytesAvailable() < qint64(sizeof(quint16)))
                flagReading = false;
            else
                in >> nextBlockSize;
        }

        if (socket->bytesAvailable() < nextBlockSize || !flagReading)
            flagReading = false;
        else
        {
            char *buffer = new char[nextBlockSize];
            in.readRawData(buffer, nextBlockSize);
            result = std::string(buffer, buffer + nextBlockSize);
            delete [] buffer;

            nextBlockSize = 0;
        }
    }
    std::cout << result << std::endl;
    message = m_protocol->fromString(result);

    return nextBlockSize;
}

int IOManager::send(QTcpSocket* socket, BaseMessage& message)
{
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);

    std::string res = m_protocol->toString(message);

    out << quint16(0);
    out.writeRawData(res.c_str(), res.size());
    out.device()->seek(0);
    out << quint16(block.size() - sizeof(quint16));

    return socket->write(block);
}
