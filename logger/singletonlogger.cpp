#include <mutex>

#include "singletonlogger.h"
#include "clientlogger.h"
#include "baselogger.h"
#include "serverlogger.h"
#include "filelog.h"

std::mutex SingletonLogger<ClientLogger>::m_mutex;
BaseLogger* SingletonLogger<ClientLogger>::m_logger = nullptr;

std::mutex SingletonLogger<ServerLogger>::m_mutex;
BaseLogger* SingletonLogger<ServerLogger>::m_logger = nullptr;

BaseLogger* SingletonLogger<ClientLogger>::getInstance()
{
    std::lock_guard<std::mutex> guard(m_mutex);

    if (m_logger == nullptr)
        m_logger = new ClientLogger(new FileLog("syslog.log"));

    return m_logger;
}

SingletonLogger<ClientLogger>::~SingletonLogger()
{
    if (m_logger != nullptr)
        delete m_logger;
}

BaseLogger* SingletonLogger<ServerLogger>::getInstance()
{
    std::lock_guard<std::mutex> guard(m_mutex);

    if (m_logger == nullptr)
        m_logger = new ServerLogger(new FileLog("syslog.log"));

    return m_logger;
}

SingletonLogger<ServerLogger>::~SingletonLogger()
{
    if (m_logger != nullptr)
        delete m_logger;
}
