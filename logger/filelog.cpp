#include <iostream>
#include <fstream>
#include <string>

#include "filelog.h"
#include "exceptoins/loggerexception.h"

FileLog::FileLog(std::string const& fileName)
{
    m_file.open(fileName, std::ios_base::app);
    if (!m_file.is_open())
        throw OpenLogException(fileName.c_str());
}

FileLog::~FileLog()
{
    if (m_file.is_open())
        m_file.close();
}

void FileLog::writeLog(std::string const& message)
{
    if (m_file.is_open())
        m_file << message << std::endl;
    else
        throw OpenLogException("*.log");
}

