#include <string>
#include <vector>
#include <QDate>
#include <QTime>
#include <stdexcept>

#include "serverlogger.h"
#include "implementlog.h"
#include "exceptoins/loggerexception.h"

ServerLogger::ServerLogger(ImplementLog* logger):
    m_logger(QSharedPointer<ImplementLog>(logger))
{

}

ServerLogger::~ServerLogger()
{

}

std::vector<std::string> ServerLogger::getLog() const
{
    throw UnsupportedMethodLog();
}

void ServerLogger::log(MessageType const type, std::string const& message)
{
    std::string dateTime = QDate().currentDate().toString().toStdString() + ": " +
                        QTime().currentTime().toString().toStdString() + ": ";

    switch (type)
    {
        case MessageType::INFO:
            m_logger->writeLog("INFO: " + dateTime + message);
            break;
        case MessageType::WARN:
            m_logger->writeLog("WARN: " + dateTime + message);
            break;
        case MessageType::ERR:
            m_logger->writeLog("ERR: " + dateTime + message);
            break;
        default:
            m_logger->writeLog("UNKNOWN PRIORITY: " + dateTime + message);
            break;
    }
}
