#ifndef SINGLETONLOGGER_H
#define SINGLETONLOGGER_H

#include <QSharedPointer>
#include <mutex>

class ServerLogger;
class ClientLogger;
class BaseLogger;

template <typename T>
class SingletonLogger
{
public:
    static BaseLogger* getInstance();

    virtual ~SingletonLogger();

    SingletonLogger(SingletonLogger const&) = delete;
    SingletonLogger& operator=(SingletonLogger const&) = delete;

private:
    SingletonLogger() = default;
    static std::mutex m_mutex;
    static BaseLogger* m_logger;
};

template <typename T>
std::mutex SingletonLogger<T>::m_mutex;

template <typename T>
BaseLogger* SingletonLogger<T>::m_logger = nullptr;

template <typename T>
BaseLogger* SingletonLogger<T>::getInstance()
{
    return nullptr;
}

template <typename T>
SingletonLogger<T>::~SingletonLogger() {

}

template <>
class SingletonLogger<ClientLogger>
{
public:
    static BaseLogger* getInstance();

    virtual ~SingletonLogger();

    SingletonLogger(SingletonLogger const&) = delete;
    SingletonLogger& operator=(SingletonLogger const&) = delete;

private:
    SingletonLogger() = default;
    static std::mutex m_mutex;
    static BaseLogger* m_logger;
};


template <>
class SingletonLogger<ServerLogger>
{
public:
    static BaseLogger* getInstance();

    virtual ~SingletonLogger();

    SingletonLogger(SingletonLogger const&) = delete;
    SingletonLogger& operator=(SingletonLogger const&) = delete;

private:
    SingletonLogger() = default;
    static std::mutex m_mutex;
    static BaseLogger* m_logger;
};

#endif // SINGLETONLOGGER_H
