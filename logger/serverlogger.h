#ifndef SERVERLOGGER_H
#define SERVERLOGGER_H

#include <QSharedPointer>
#include <string>
#include <vector>

#include "baselogger.h"

class ImplementLog;

class ServerLogger: public BaseLogger
{
public:
    explicit ServerLogger(ImplementLog* logger);
    ~ServerLogger();

    void log(MessageType const type, std::string const& message);
    std::vector<std::string> getLog() const;

private:
    QSharedPointer<ImplementLog> m_logger;
};

#endif // SERVERLOGGER_H
