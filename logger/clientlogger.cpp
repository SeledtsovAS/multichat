#include <QTime>
#include <string>
#include <vector>

#include "clientlogger.h"
#include "implementlog.h"

#include "exceptoins/loggerexception.h"

ClientLogger::ClientLogger(ImplementLog* logger):
    m_logger(QSharedPointer<ImplementLog>(logger))
{

}

ClientLogger::~ClientLogger()
{
    for (auto message: m_history)
        m_logger->writeLog(message);
}

void ClientLogger::log(MessageType const type, std::string const& message)
{
    if (m_logger.data() == nullptr)
        throw NotFoundImplementLog();

    std::string time = QTime().currentTime().toString().toStdString();
    std::string date = QDate().currentDate().toString().toStdString();

    switch (type)
    {
        case MessageType::INFO:
            m_history.push_back("INFO: " + date + ": " + time + ": " + message);
            break;
        case MessageType::WARN:
            m_history.push_back("WARN: " + date + ": " + time + ": " + message);
            break;
        case MessageType::ERR:
            m_history.push_back("ERR: " + date + ": " + time + ": " + message);
            break;
        default:
            m_history.push_back("UNKNOWN PRIORITY: " + date + ": " + time + ": " + message);
            break;
    }
}

std::vector<std::string> ClientLogger::getLog() const
{
    return m_history;
}
