#ifndef IMPLEMENTLOG_H
#define IMPLEMENTLOG_H

#include <string>

class ImplementLog
{
public:
    virtual ~ImplementLog() = 0;

    virtual void writeLog(std::string const& message) = 0;
};

#endif // IMPLEMENTLOG_H
