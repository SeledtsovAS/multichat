#ifndef FILELOG_H
#define FILELOG_H

#include <fstream>
#include <string>

#include "implementlog.h"

class QString;

class FileLog: public ImplementLog
{
public:
    explicit FileLog(std::string const& fileName);
    ~FileLog();

    void writeLog(std::string const& message);

private:
    std::ofstream m_file;
};

#endif // FILELOG_H
