#ifndef BASELOGER_H
#define BASELOGER_H

#include <string>
#include <vector>

class BaseLogger
{
public:
    enum class MessageType: char
    {
        INFO = 0x001,
        WARN = 0x002,
        ERR = 0x003
    };

public:
    virtual ~BaseLogger() = 0;

    virtual void log(MessageType const type, std::string const& message) = 0;
    virtual std::vector<std::string> getLog() const = 0;
};

#endif // BASELOGER_H
