#ifndef CLIENTLOGGER_H
#define CLIENTLOGGER_H

#include <QSharedPointer>
#include <vector>
#include <string>

#include "baselogger.h"

class ImplementLog;

class ClientLogger: public BaseLogger
{
public:
    explicit ClientLogger(ImplementLog* logger);
    ~ClientLogger();

    void log(MessageType const type, std::string const& message);
    std::vector<std::string> getLog() const;

private:
    QSharedPointer<ImplementLog> m_logger;
    std::vector<std::string> m_history;
};

#endif // CLIENTLOGGER_H
