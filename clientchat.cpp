#include <fstream>
#include <iostream>

#include <QString>
#include <QtNetwork/QTcpSocket>
#include <QSharedPointer>
#include <QHostAddress>
#include <QAbstractSocket>
#include <QMessageBox>
#include <QFileDialog>
#include <QInputDialog>

#include "clientchat.h"

#include "io/baseiomanager.h"
#include "io/criptodecoratorio.h"
#include "io/iomanager.h"
#include "io/decoratorfilter.h"

#include "security/cryptoppsystem.h"
#include "security/sslalgorithmfactory.h"
#include "security/cryptoppalgorithmfactory.h"

#include "messages/basemessage.h"
#include "messages/message.h"
#include "messages/messageansweronline.h"
#include "messages/messageonline.h"
#include "messages/messageaccess.h"
#include "messages/messagegetaccess.h"
#include "messages/messagefile.h"

#include "protocol/bufferprotocolimpl.h"
#include "protocol/protocolstrategy.h"
#include "protocol/protocolimpl.h"
#include "protocol/textprotocolimpl.h"

#include "logger/filelog.h"
#include "logger/clientlogger.h"
#include "logger/singletonlogger.h"

ClientChat::ClientChat(QMainWindow* parent):
    QMainWindow(parent)
{
    setupUi(this);

    m_socket = QSharedPointer<QTcpSocket>(new QTcpSocket(this));
    m_logger = QSharedPointer<BaseLogger>(SingletonLogger<ClientLogger>::getInstance());

    connect(m_socket.data(), SIGNAL(connected()), this, SLOT(slotConnected()));
    connect(m_socket.data(), SIGNAL(readyRead()), this, SLOT(slotReadyRead()));
    connect(m_socket.data(), SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(slotError(QAbstractSocket::SocketError)));
    connect(m_socket.data(), SIGNAL(disconnected()), this, SLOT(slotDisconnect()));


    handlerMapper[BaseMessage::TypeMessage::LIST_ONLINE] = &ClientChat::handlerMessageOnline;
    handlerMapper[BaseMessage::TypeMessage::SMS] = &ClientChat::handlerMessageSMS;
    handlerMapper[BaseMessage::TypeMessage::FILE] = &ClientChat::handlerMessageFile;
    handlerMapper[BaseMessage::TypeMessage::GET_ACCESS] = &ClientChat::handlerMessageGetAccess;
    handlerMapper[BaseMessage::TypeMessage::ACCESS] = &ClientChat::handlerMessageAccess;

    m_isConnection = false;
}

ClientChat::~ClientChat() 
{
    m_socket->disconnectFromHost();
}

void ClientChat::slotError(QAbstractSocket::SocketError) 
{
    QMessageBox::information(this, "Error connection", "Cannot connection in host", QMessageBox::Ok);
}

void ClientChat::updateTxtLog()
{
    txtLog->clear();
    for (const auto& line: m_logger->getLog())
        txtLog->append(line.c_str());
}

void ClientChat::slotConnected() {
    m_logger->log(BaseLogger::MessageType::INFO, "Chat подключился к хосту" +
                  m_socket->peerAddress().toString().toStdString() + " к порту" +
                  QString::number(m_socket->peerPort()).toStdString());

    m_isConnection = true;
    txtMessages_2->append("connected");
    updateTxtLog();
}

void ClientChat::handlerMessageOnline(QSharedPointer<BaseMessage> message) {
    m_logger->log(BaseLogger::MessageType::INFO, "Получено сообщение типа ListOnline от хоста " +
                  m_socket->peerAddress().toString().toStdString() + " от порта " +
                  QString::number(m_socket->peerPort()).toStdString());
    listClients_2->clear();

    QStringList lstId = QString(message->getBody().c_str()).split(" ");
    if (lstId.size() == 1 && lstId[0] == "0") return;
    for (const auto& elem: lstId)
        listClients_2->addItem("Client: " + elem);
}

void ClientChat::handlerMessageSMS(QSharedPointer<BaseMessage> message) {
    m_logger->log(BaseLogger::MessageType::INFO, "Получено сообщение типа SMS от хоста " +
                  m_socket->peerAddress().toString().toStdString() + " от порта " +
                  QString::number(m_socket->peerPort()).toStdString());
    m_logger->log(BaseLogger::MessageType::INFO, "Сообщение содержит: " + message->getBody());

    txtMessages_2->append("Client" + QString::number(message->getFrom()) +
                        " say : " + QString(message->getBody().c_str()));
}

void ClientChat::handlerMessageFile(QSharedPointer<BaseMessage> message) {
    std::string namefile = QInputDialog::getText(this, "Input name file", "Input name save file:").toStdString();
    if (namefile == "") return;

    m_logger->log(BaseLogger::MessageType::INFO, "Получено сообщение типа файл от хоста " +
                  m_socket->peerAddress().toString().toStdString() + " от порта " +
                  QString::number(m_socket->peerPort()).toStdString());
    m_logger->log(BaseLogger::MessageType::INFO, "Файл сохранен под именем: " + message->getTitle());

    txtMessages_2->append("Client" + QString::number(message->getFrom()) + " send file: " + QString(namefile.c_str()));
    std::ofstream src(namefile);
    if (src.is_open()) {
        src.write(message->getBody().c_str(), message->getBody().size());
        src.close();
    }
}

void ClientChat::handlerMessageGetAccess(QSharedPointer<BaseMessage> message) {
    m_logger->log(BaseLogger::MessageType::INFO, "Получено запрос на передачу сообщения от клиента " +
                  QString::number(message->getFrom()).toStdString());

    int res = QMessageBox::question(this, "Передача данных", "Вы хотите принять сообщение?",
                                    QMessageBox::Ok, QMessageBox::Cancel);
    std::string answer = res == QMessageBox::Ok ? "y" : "n";

    MessageAccess access(message->getFrom(), message->getTo(), answer);
    access.setTitle(message->getTitle());
    slotSendToServer(access);
}

void ClientChat::handlerMessageAccess(QSharedPointer<BaseMessage> message) {
    m_logger->log(BaseLogger::MessageType::WARN, "Получен ответ от клиента " + QString::number(message->getFrom()).toStdString() +
                  " содержащий сообщение о прерванном сообщении.");
    QMessageBox::information(this, "Передача данных", "Передача данных была прервана.", QMessageBox::Ok);
}

void ClientChat::slotReadyRead()
{
    if (!m_isConnection) return;

    QSharedPointer<BaseMessage> message = QSharedPointer<BaseMessage>(nullptr);
    m_ioManager->recv(m_socket.data(), message);

    handlerMapper[message->getType()](*this, message);
    updateTxtLog();
}

void ClientChat::slotSendToServer(BaseMessage& message)
{
    if (!m_isConnection) return;

    m_logger->log(BaseLogger::MessageType::INFO, "Начало передачи");
    m_ioManager->send(m_socket.data(), message);
    m_logger->log(BaseLogger::MessageType::INFO, "Передача данных завершена");
    updateTxtLog();
}

void ClientChat::slotDisconnect()
{
    m_isConnection = false;
    m_logger->log(BaseLogger::MessageType::INFO, "Соединение разорвано!");
    txtMessages_2->append("disconnect");
    updateTxtLog();
}

void ClientChat::on_btnConnect_2_clicked()
{
    if (m_isConnection) return;
    if (edtHost_2->text().isEmpty() || edtPort_2->text().isEmpty()) return;

    ProtocolImpl* protocol1 = nullptr;
    ProtocolImpl* protocol2 = nullptr;

    if (radioBuffer_2->isChecked()) {
        protocol1 = new BufferProtocolImpl;
        protocol2 = new BufferProtocolImpl;
    }
    else
    {
        protocol1 = new TextProtocolImpl;
        protocol2 = new TextProtocolImpl;
    }

    CryptoPPSystem* sysCr = new CryptoPPSystem(new SSLAlgorithmFactory);
    BaseIOManager* crypto = new CriptoDecoratorIO(new IOManager(new ProtocolStrategy(protocol1)), sysCr);
    BaseIOManager* ioManager = new IOManager(new ProtocolStrategy(protocol2));
    m_ioManager = QSharedPointer<BaseIOManager>(new DecoratorFilter(crypto, ioManager));

    m_socket->connectToHost(edtHost_2->text(), edtPort_2->text().toInt());
}

void ClientChat::on_btnDisconnect_2_clicked() {
    m_socket->disconnectFromHost();
}

void ClientChat::on_btnSend_2_clicked()
{
    if (!m_isConnection) return;
    if (edtMessage_2->text().isEmpty()) return;
    if (listClients_2->currentItem() == nullptr) return;

    QString to = listClients_2->currentItem()->text().split(" ")[1];
    Message message(0, to.toInt());
    message.setBody(edtMessage_2->text().toStdString());

    slotSendToServer(message);
}

void ClientChat::on_btnClientsUpdate_2_clicked()
{
    if (!m_isConnection) return;

    m_logger->log(BaseLogger::MessageType::INFO, "Запрос у сервера данных о клиентах онлайн.");
    MessageAnswerOnline message(-1, -1);
    slotSendToServer(message);

    updateTxtLog();
}

void ClientChat::on_btnSendFile_2_clicked()
{
    std::string filename = QFileDialog::getOpenFileName(this, "Load file", ".").toStdString();
    if (listClients_2->currentItem() == nullptr) return;
    if (filename == "") return;

    QString to = listClients_2->currentItem()->text().split(" ")[1];
    MessageFile message(0, to.toInt(), filename);
    message.load();

    slotSendToServer(message);
}
