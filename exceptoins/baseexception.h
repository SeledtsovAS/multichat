#ifndef BASEEXCEPTION_H
#define BASEEXCEPTION_H

#include <exception>

class BaseException: public std::exception
{
public:
    virtual ~BaseException() = 0;
    const char* what() const noexcept = 0;
};

#endif // BASEEXCEPTION_H
