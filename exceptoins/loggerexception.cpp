#include <string>

#include "loggerexception.h"

LoggerException::~LoggerException()
{
}

OpenLogException::OpenLogException(char const* fileName):
    m_fileName(fileName)
{
}

OpenLogException::~OpenLogException()
{
}

char const* OpenLogException::what() const noexcept
{
    return (std::string("Cannot open Logfile: ") + m_fileName).c_str();
}


NotFoundImplementLog::~NotFoundImplementLog()
{
}

char const* NotFoundImplementLog::what() const noexcept
{
    return "ImplementLog from *logger is nullptr";
}

UnsupportedMethodLog::~UnsupportedMethodLog()
{
}

char const* UnsupportedMethodLog::what() const noexcept
{
    return "ServerLog is not support method getLog()";
}
