#include <string>

#include "messageexception.h"

MessageException::~MessageException()
{
}

MessageNotFoundMethod::MessageNotFoundMethod(const std::string &method)
{
    m_method = method;
}

MessageNotFoundMethod::~MessageNotFoundMethod()
{

}

char const* MessageNotFoundMethod::what() const noexcept
{
    return ("Cannot found method " + m_method + ".").c_str();
}
