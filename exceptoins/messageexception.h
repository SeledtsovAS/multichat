#ifndef MESSAGEEXCEPTION_H
#define MESSAGEEXCEPTION_H

#include <string>

#include "baseexception.h"

class MessageException: public BaseException
{
public:
    virtual ~MessageException();

    virtual char const* what() const noexcept = 0;
};

class MessageNotFoundMethod: public MessageException
{
public:
    MessageNotFoundMethod(std::string const& method);
    ~MessageNotFoundMethod();

    char const* what() const noexcept;

private:
    std::string m_method;
};

#endif // MESSAGEEXCEPTION_H
