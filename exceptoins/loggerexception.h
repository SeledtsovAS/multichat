#ifndef LOGGEREXCEPTION_H
#define LOGGEREXCEPTION_H

#include "baseexception.h"

class LoggerException: public BaseException
{
public:
    ~LoggerException();

    char const* what() const noexcept = 0;
};

class OpenLogException: public LoggerException
{
public:
    OpenLogException(char const* fileName);
    ~OpenLogException();

    char const* what() const noexcept;

private:
    char const* m_fileName;
};

class NotFoundImplementLog: public LoggerException
{
public:
    ~NotFoundImplementLog();

    char const* what() const noexcept;
};

class UnsupportedMethodLog: public LoggerException
{
public:
    ~UnsupportedMethodLog();

    char const* what() const noexcept;
};

#endif // LOGGEREXCEPTION_H
