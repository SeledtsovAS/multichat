#include <QSharedPointer>
#include <QTcpServer>
#include <QTcpSocket>
#include <QMessageBox>
#include <QDebug>
#include <QDataStream>
#include <QByteArray>
#include <QMessageBox>
#include <iostream>

#include "tcpserver.h"

#include "io/baseiomanager.h"
#include "io/iomanager.h"
#include "io/criptodecoratorio.h"
#include "io/decoratorfilter.h"

#include "logger/baselogger.h"
#include "logger/serverlogger.h"

#include "security/cryptoppsystem.h"
#include "security/cryptoppalgorithmfactory.h"
#include "security/sslalgorithmfactory.h"

#include "protocol/protocolstrategy.h"
#include "protocol/protocolimpl.h"
#include "protocol/bufferprotocolimpl.h"
#include "protocol/textprotocolimpl.h"

#include "messages/basemessage.h"
#include "messages/message.h"
#include "messages/messageansweronline.h"
#include "messages/messageonline.h"
#include "messages/messageaccess.h"
#include "messages/messagegetaccess.h"

unsigned int TCPServer::m_nClients = 1;
unsigned int TCPServer::m_nObjects = 0;

TCPServer::TCPServer(const int port, BaseLogger* logger, QWidget* parent):
    QWidget(parent), m_logger(QSharedPointer<BaseLogger>(logger)),
    m_nextBlockSize(0)
{
    m_logger->log(BaseLogger::MessageType::INFO, "Начало сеанса работы сервера.");

    ProtocolImpl* protocol1 = new TextProtocolImpl;
    ProtocolImpl* protocol2 = new TextProtocolImpl;

    CryptoPPSystem* sysCr = new CryptoPPSystem(new SSLAlgorithmFactory);
    BaseIOManager* crypto = new CriptoDecoratorIO(new IOManager(new ProtocolStrategy(protocol1)), sysCr);
    BaseIOManager* ioManager = new IOManager(new ProtocolStrategy(protocol2));
    m_ioManager = QSharedPointer<BaseIOManager>(new DecoratorFilter(crypto, ioManager));



    m_server = QSharedPointer<QTcpServer>(new QTcpServer(this));
    if (m_server->listen(QHostAddress::LocalHost, port) == false) {
        m_logger->log(BaseLogger::MessageType::ERR, "Не могу начать прослушивать порт: " +
                     QString::number(port).toStdString() + ".");
        m_logger->log(BaseLogger::MessageType::INFO, "Конец сеанса работы сервера.");

        m_server->close();
        return;
    }

    m_logger->log(BaseLogger::MessageType::INFO, "Установлен обработчик на сигнал newConnection().");
    connect(m_server.data(), SIGNAL(newConnection()), this, SLOT(slotNewConnection()));

    handlerMapper[BaseMessage::TypeMessage::GET_LIST_ONLINE] = &TCPServer::handlerMessageOnline;
    handlerMapper[BaseMessage::TypeMessage::FILE] = &TCPServer::handlerMessageSmsOrFile;
    handlerMapper[BaseMessage::TypeMessage::SMS] = &TCPServer::handlerMessageSmsOrFile;
    handlerMapper[BaseMessage::TypeMessage::ACCESS] = &TCPServer::handlerMessageAccess;

}

TCPServer::~TCPServer()
{
    m_logger->log(BaseLogger::MessageType::INFO, "Конец сеанса работы сервера.");
}

void TCPServer::slotNewConnection()
{
    QTcpSocket* socket = m_server->nextPendingConnection();
    m_logger->log(BaseLogger::MessageType::INFO, "Клиент c ip: " + socket->peerAddress().toString().toStdString() +
                  " и портом " + QString::number(socket->peerPort()).toStdString() + " подключился.");

    connect(socket, SIGNAL(disconnected()), this, SLOT(slotDisconnect()));
    connect(socket, SIGNAL(disconnected()), socket, SLOT(deleteLater()));
    connect(socket, SIGNAL(readyRead()), this, SLOT(slotClientRead()));

    m_logger->log(BaseLogger::MessageType::INFO, "Новый клиент был добавлен в список подключенных.");
    m_clients[m_nClients++] = socket;
}

void TCPServer::slotDisconnect()
{
    QTcpSocket* socket = qobject_cast<QTcpSocket*>(sender());

    m_logger->log(BaseLogger::MessageType::INFO, "Клиент c ip: " + socket->peerAddress().toString().toStdString() +
                  " и портом " + QString::number(socket->peerPort()).toStdString() + " отключился.");

    for (const auto& pair: m_clients)
        if (pair.second == socket)
            m_clients.erase(m_clients.find(pair.first));
}

std::vector<int> TCPServer::getVectorClients(QTcpSocket *socket)
{
    std::vector<int> result { };
    for (const auto& pair: m_clients) {
        if (pair.second != socket)
            result.push_back(pair.first);
    }

    return result;
}

int TCPServer::getId(QTcpSocket *socket)
{
    for (const auto& pair: m_clients)
        if (pair.second == socket)
            return pair.first;
    return -1;
}

void TCPServer::handlerMessageOnline(QSharedPointer<BaseMessage>, QTcpSocket* socket)
{
    MessageOnline responce(-1, 0, getVectorClients(socket));
    sendToClient(socket, responce);
}

void TCPServer::handlerMessageSmsOrFile(QSharedPointer<BaseMessage> message, QTcpSocket* socket)
{
    message->setFrom(getId(socket));
    MessageGetAccess getAccess(message->getFrom(), message->getTo(), m_nObjects);
    sendToClient(m_clients[message->getTo()], getAccess);
    m_objects[m_nObjects] = message;
    m_nObjects++;
}

void TCPServer::handlerMessageAccess(QSharedPointer<BaseMessage> message, QTcpSocket*)
{
    QSharedPointer<BaseMessage> objMes = m_objects[QString(message->getTitle().c_str()).toInt()];
    if (message->getBody() == "y")
        sendToClient(m_clients[objMes->getTo()], *objMes);
    else {
        MessageAccess access(objMes->getFrom(), objMes->getTo(), "n");
        sendToClient(m_clients[objMes->getFrom()], access);
    }

    m_objects.erase(QString(message->getTitle().c_str()).toInt());
}

void TCPServer::slotClientRead()
{
    QTcpSocket* socket = qobject_cast<QTcpSocket*>(sender());
    QSharedPointer<BaseMessage> message = QSharedPointer<BaseMessage>(nullptr);
    m_nextBlockSize = m_ioManager->recv(socket, message);

    handlerMapper[message->getType()](*this, message, socket);
}

void TCPServer::sendToClient(QTcpSocket *sock, BaseMessage& message)
{
    m_ioManager->send(sock, message);
}
