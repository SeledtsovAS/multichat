#ifndef SYMMETRICALGORITHM_H
#define SYMMETRICALGORITHM_H

#include <string>

class SymmetricAlgorithm
{
public:
    virtual ~SymmetricAlgorithm() = 0;

    virtual void encrypt(std::string& key, std::string& message) = 0;
    virtual void decrypt(std::string const& key, std::string& message) = 0;
};

#endif // SYMMETRICALGORITHM_H
