#ifndef ALGORITHMSSLDES_H
#define ALGORITHMSSLDES_H

#include "symmetricalgorithm.h"

class AlgorithmSslDES: public SymmetricAlgorithm
{
public:
    ~AlgorithmSslDES();

    void decrypt(std::string const& key, std::string& message);
    void encrypt(std::string& key, std::string& message);
};

#endif // ALGORITHMSSLDES_H
