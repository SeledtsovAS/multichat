#include "asymmetricalgorithm.h"
#include "symmetricalgorithm.h"
#include "algorithmdes.h"
#include "algorithmrsa.h"
#include "encryptkeys.h"

#include "cryptoppalgorithmfactory.h"

CryptoPPAlgorithmFactory::~CryptoPPAlgorithmFactory() {

}

AsymmetricAlgorithm* CryptoPPAlgorithmFactory::getAsymmetricAlgorithm() const
{
    return new AlgorithmRSA(EncryptKeys::publicKey, EncryptKeys::privateKey);
}

SymmetricAlgorithm* CryptoPPAlgorithmFactory::getSymmetricAlgorithm() const
{
    return new AlgorithmDES;
}
