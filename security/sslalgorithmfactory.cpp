#include "symmetricalgorithm.h"
#include "asymmetricalgorithm.h"
#include "algorithmssldes.h"
#include "algorithmsslrsa.h"
#include "encryptkeys.h"

#include "sslalgorithmfactory.h"

SSLAlgorithmFactory::~SSLAlgorithmFactory() {

}

AsymmetricAlgorithm* SSLAlgorithmFactory::getAsymmetricAlgorithm() const
{
    return new AlgorithmSslRSA(EncryptKeys::pubKey, EncryptKeys::privKey);
}

SymmetricAlgorithm* SSLAlgorithmFactory::getSymmetricAlgorithm() const
{
    return new AlgorithmSslDES;
}


