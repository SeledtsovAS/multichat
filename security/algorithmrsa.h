#ifndef ALGORITHMRSA_H
#define ALGORITHMRSA_H

#include <crypto++/rsa.h>
#include <string>

#include "asymmetricalgorithm.h"

class AlgorithmRSA: public AsymmetricAlgorithm
{
public:
    AlgorithmRSA(std::string const& pubKey, std::string const& privKey);
    virtual ~AlgorithmRSA();

    virtual void encrypt(std::string& message);
    virtual void decrypt(std::string& message);

private:
    CryptoPP::RSAES_OAEP_SHA_Decryptor m_decryptor;
    CryptoPP::RSAES_OAEP_SHA_Encryptor m_encryptor;
};

#endif // ALGORITHMRSA_H
