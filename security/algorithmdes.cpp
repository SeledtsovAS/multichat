#include <crypto++/base64.h>
#include <crypto++/osrng.h>
#include <crypto++/des.h>
#include <crypto++/modes.h>

#include <string>

#include "algorithmdes.h"

#define unused(var) static_cast<void>(var)

AlgorithmDES::~AlgorithmDES() {

}

void AlgorithmDES::encrypt(std::string& key, std::string& message)
{
    CryptoPP::AutoSeededRandomPool rand;
    CryptoPP::SecByteBlock blockKey(0, CryptoPP::DES_EDE2::DEFAULT_KEYLENGTH);
    rand.GenerateBlock(blockKey, blockKey.size());

    byte iv[CryptoPP::DES_EDE2::BLOCKSIZE] { 0 };
    CryptoPP::CBC_Mode<CryptoPP::DES_EDE2>::Encryption encriptor;
    encriptor.SetKeyWithIV(blockKey, blockKey.size(), iv);

    std::string result = "";
    CryptoPP::StringSource transform(message, true, new CryptoPP::StreamTransformationFilter(encriptor,
                                                                      new CryptoPP::StringSink(result)));
    unused(transform);
    message = result;
    key = std::string(blockKey.begin(), blockKey.end());
}

void AlgorithmDES::decrypt(std::string const& key, std::string& message)
{
    CryptoPP::SecByteBlock blockKey(0, CryptoPP::DES_EDE2::DEFAULT_KEYLENGTH);
    blockKey.Assign(reinterpret_cast<const byte*>(key.c_str()), blockKey.size());

    byte iv[CryptoPP::DES_EDE2::BLOCKSIZE] { 0 };
    CryptoPP::CBC_Mode<CryptoPP::DES_EDE2>::Decryption decryptor;
    decryptor.SetKeyWithIV(blockKey, blockKey.size(), iv);

    std::string result = "";
    CryptoPP::StringSource transform(message, true, new CryptoPP::StreamTransformationFilter(decryptor,
                                                                      new CryptoPP::StringSink(result)));
    unused(transform);
    message = result;
}
