#ifndef ALGORITHMDES_H
#define ALGORITHMDES_H

#include <string>

#include "symmetricalgorithm.h"

class AlgorithmDES: public SymmetricAlgorithm
{
public:
    ~AlgorithmDES();

    void decrypt(std::string const& key, std::string& message);
    void encrypt(std::string& key, std::string& message);
};

#endif // ALGORITHMDES_H
