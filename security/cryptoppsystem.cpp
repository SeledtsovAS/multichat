#include <QSharedPointer>

#include <string>

#include "cryptoppsystem.h"
#include "asymmetricalgorithm.h"
#include "symmetricalgorithm.h"
#include "abstractfactory.h"

CryptoPPSystem::CryptoPPSystem(AbstractFactory* factory):
    m_symmetricAlg(QSharedPointer<SymmetricAlgorithm>(factory->getSymmetricAlgorithm())),
    m_asymmetricAlg(QSharedPointer<AsymmetricAlgorithm>(factory->getAsymmetricAlgorithm())) {

    delete factory;
}

CryptoPPSystem::~CryptoPPSystem() {

}

void CryptoPPSystem::decrypt(std::string& message)
{
    m_asymmetricAlg->decrypt(message);
}

void CryptoPPSystem::encrypt(std::string& message)
{
    m_asymmetricAlg->encrypt(message);
}

void CryptoPPSystem::decrypt(std::string const& key, std::string& message)
{
    m_symmetricAlg->decrypt(key, message);
}

void CryptoPPSystem::encrypt(std::string& key, std::string& message)
{
    m_symmetricAlg->encrypt(key, message);
}
