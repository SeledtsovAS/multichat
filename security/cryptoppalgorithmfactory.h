#ifndef CRYPTOPPALGORITHMFACTORY_H
#define CRYPTOPPALGORITHMFACTORY_H

#include "abstractfactory.h"

class SymmetricAlgorithm;
class AsymmetricAlgorithm;

class CryptoPPAlgorithmFactory: public AbstractFactory
{
public:
    virtual ~CryptoPPAlgorithmFactory();

    SymmetricAlgorithm* getSymmetricAlgorithm() const;
    AsymmetricAlgorithm* getAsymmetricAlgorithm() const;
};

#endif // CRYPTOPPALGORITHMFACTORY_H
