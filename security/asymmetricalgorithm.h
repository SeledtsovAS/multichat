#ifndef ASYMMETRICALGORITHM_H
#define ASYMMETRICALGORITHM_H

#include <string>

class AsymmetricAlgorithm
{
public:
    virtual ~AsymmetricAlgorithm() = 0;

    virtual void encrypt(std::string& message) = 0;
    virtual void decrypt(std::string& message) = 0;
};

#endif // ASYMMETRICALGORITHM_H
