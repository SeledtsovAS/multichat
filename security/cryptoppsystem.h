#ifndef CRYPTOPPSYSTEM_H
#define CRYPTOPPSYSTEM_H

#include <QSharedPointer>
#include <string>

class SymmetricAlgorithm;
class AsymmetricAlgorithm;
class AbstractFactory;

class CryptoPPSystem
{
public:
    CryptoPPSystem(AbstractFactory* factory);
    virtual ~CryptoPPSystem();

    void encrypt(std::string& key, std::string& message);
    void decrypt(std::string const& key, std::string& message);

    void encrypt(std::string& message);
    void decrypt(std::string& message);

private:
    QSharedPointer<SymmetricAlgorithm> m_symmetricAlg;
    QSharedPointer<AsymmetricAlgorithm> m_asymmetricAlg;
};

#endif // CRYPTOPPSYSTEM_H
