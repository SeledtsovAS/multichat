#include <string>
#include <openssl/pem.h>
#include <openssl/ssl.h>
#include <openssl/rsa.h>
#include <openssl/evp.h>
#include <openssl/bio.h>
#include <openssl/err.h>
#include <cstring>

#include "algorithmsslrsa.h"

AlgorithmSslRSA::AlgorithmSslRSA(const std::string &keyPub, const std::string& keyPriv)
{
    m_pubKey = keyPub;
    m_privKey = keyPriv;
}

AlgorithmSslRSA::~AlgorithmSslRSA() {

}

RSA* AlgorithmSslRSA::loadPublicKeyFromString(const char* key)
{
    BIO* bio = BIO_new_mem_buf((void*)key, -1);
    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);

    RSA* rsa = PEM_read_bio_RSAPublicKey(bio, nullptr, nullptr, nullptr);
    BIO_free(bio);
    return rsa;
}

RSA* AlgorithmSslRSA::loadPrivateKeyFromString(const char* key)
{
    BIO* bio = BIO_new_mem_buf((void*)key, -1);
    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);

    RSA* rsa = PEM_read_bio_RSAPrivateKey(bio, nullptr, nullptr, nullptr);
    BIO_free(bio);
    return rsa;
}

void AlgorithmSslRSA::decrypt(std::string &message)
{
    RSA* rsaPub = loadPrivateKeyFromString(m_privKey.c_str());
    int rsaSize = RSA_size(rsaPub);

    unsigned char* ed = new unsigned char[rsaSize] { };
    RSA_private_decrypt(message.size(), reinterpret_cast<const unsigned char*>(message.c_str()),
                       ed, rsaPub, RSA_PKCS1_OAEP_PADDING);

    message = std::string(ed, ed + rsaSize);
}

void AlgorithmSslRSA::encrypt(std::string &message)
{
    RSA* rsaPub = loadPrivateKeyFromString(m_privKey.c_str());
    int rsaSize = RSA_size(rsaPub);

    unsigned char* ed = new unsigned char[rsaSize] { };
    RSA_public_encrypt(message.size(), reinterpret_cast<const unsigned char*>(message.c_str()),
                       ed, rsaPub, RSA_PKCS1_OAEP_PADDING);

    message = std::string(ed, ed + rsaSize);
}
