#include "encryptkeys.h"

std::string EncryptKeys::privateKey = R"(MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAMx1k+XIYeg31+zqIAolW9K8
        zHGPiOQvAP7svTFj0rMj7aLbYZJ5hSPDb8szIjQeiLnjE543l5fanpVkTiZE5W/TWDtXxaxV
        Uzwj7YEF88KW98Sf69m3Hhl9LMyQSnzesLNjnc7hEQRhfU2J9RY5Xj8nHf7VIyrLBw87e1xf
        aFjRAgERAoGAEAk4xrpiCCyJZ+o0tYBrmA7Ox6bdj2MPIw2MVC/8cnE/0ImtTL48o26zb09N
        +gxvHaNg6UWddVJmynZMZ2TGs0wGDfgzA12IJQBNBZfEiGeaeOy9zv4Dqm9XaGP8b5QFb6D3
        kP+mw96d17gmqpJ0R37vUuEdwTI+X7mhxSnvHjECQQDvedlOI90uOYtAIukU2sO8TxfOW+Vd
        JaoTQSJZv8D1zwFaBQbxtWhifN4exIixvG80r5oMg1m3x/OwHG2l6hPtAkEA2pEv7RdkPpHZ
        H/SQog8zsbcnBxvlM0myXlE3O7gtSZxixURPM81aqKPw/0OOvFdAYAMEEmRx0qPXDJBc4tWD
        9QJBALcg8Xf9Tsj+0+XAV+LFaIDxMFKCgjgr3GkTsNs4SEOALjXHm+YDMbS51wh4LEuuNuwN
        0CevvRQRX/8kzFGzAC0CQE0kLwhim9nZH3Swq4R91gJ84JkY53uDbCFJ10JBAOzNyIHb37fu
        H/9I66VjI1GINNaXpxWM3ODuiCKNTfW0xSkCQQDNfb9omWor2paPpS+pmBs00w1rFXGijLIa
        PveuDPSktvlGZjp3Ld0THZ42Hr3ogYdh7uc3o3Rstwvx7IXQsCXR)";

std::string EncryptKeys::publicKey = R"(MIGdMA0GCSqGSIb3DQEBAQUAA4GLADCBhwKBgQDMdZPlyGHoN9fs6iAKJVvSvMxxj4jkLwD+
        7L0xY9KzI+2i22GSeYUjw2/LMyI0Hoi54xOeN5eX2p6VZE4mROVv01g7V8WsVVM8I+2BBfPC
        lvfEn+vZtx4ZfSzMkEp83rCzY53O4REEYX1NifUWOV4/Jx3+1SMqywcPO3tcX2hY0QIBEQ==)";

std::string EncryptKeys::privKey = "-----BEGIN RSA PRIVATE KEY-----\n"\
        "MIIEowIBAAKCAQEAy8Dbv8prpJ/0kKhlGeJYozo2t60EG8L0561g13R29LvMR5hy\n"\
        "vGZlGJpmn65+A4xHXInJYiPuKzrKUnApeLZ+vw1HocOAZtWK0z3r26uA8kQYOKX9\n"\
        "Qt/DbCdvsF9wF8gRK0ptx9M6R13NvBxvVQApfc9jB9nTzphOgM4JiEYvlV8FLhg9\n"\
        "yZovMYd6Wwf3aoXK891VQxTr/kQYoq1Yp+68i6T4nNq7NWC+UNVjQHxNQMQMzU6l\n"\
        "WCX8zyg3yH88OAQkUXIXKfQ+NkvYQ1cxaMoVPpY72+eVthKzpMeyHkBn7ciumk5q\n"\
        "gLTEJAfWZpe4f4eFZj/Rc8Y8Jj2IS5kVPjUywQIDAQABAoIBADhg1u1Mv1hAAlX8\n"\
        "omz1Gn2f4AAW2aos2cM5UDCNw1SYmj+9SRIkaxjRsE/C4o9sw1oxrg1/z6kajV0e\n"\
        "N/t008FdlVKHXAIYWF93JMoVvIpMmT8jft6AN/y3NMpivgt2inmmEJZYNioFJKZG\n"\
        "X+/vKYvsVISZm2fw8NfnKvAQK55yu+GRWBZGOeS9K+LbYvOwcrjKhHz66m4bedKd\n"\
        "gVAix6NE5iwmjNXktSQlJMCjbtdNXg/xo1/G4kG2p/MO1HLcKfe1N5FgBiXj3Qjl\n"\
        "vgvjJZkh1as2KTgaPOBqZaP03738VnYg23ISyvfT/teArVGtxrmFP7939EvJFKpF\n"\
        "1wTxuDkCgYEA7t0DR37zt+dEJy+5vm7zSmN97VenwQJFWMiulkHGa0yU3lLasxxu\n"\
        "m0oUtndIjenIvSx6t3Y+agK2F3EPbb0AZ5wZ1p1IXs4vktgeQwSSBdqcM8LZFDvZ\n"\
        "uPboQnJoRdIkd62XnP5ekIEIBAfOp8v2wFpSfE7nNH2u4CpAXNSF9HsCgYEA2l8D\n"\
        "JrDE5m9Kkn+J4l+AdGfeBL1igPF3DnuPoV67BpgiaAgI4h25UJzXiDKKoa706S0D\n"\
        "4XB74zOLX11MaGPMIdhlG+SgeQfNoC5lE4ZWXNyESJH1SVgRGT9nBC2vtL6bxCVV\n"\
        "WBkTeC5D6c/QXcai6yw6OYyNNdp0uznKURe1xvMCgYBVYYcEjWqMuAvyferFGV+5\n"\
        "nWqr5gM+yJMFM2bEqupD/HHSLoeiMm2O8KIKvwSeRYzNohKTdZ7FwgZYxr8fGMoG\n"\
        "PxQ1VK9DxCvZL4tRpVaU5Rmknud9hg9DQG6xIbgIDR+f79sb8QjYWmcFGc1SyWOA\n"\
        "SkjlykZ2yt4xnqi3BfiD9QKBgGqLgRYXmXp1QoVIBRaWUi55nzHg1XbkWZqPXvz1\n"\
        "I3uMLv1jLjJlHk3euKqTPmC05HoApKwSHeA0/gOBmg404xyAYJTDcCidTg6hlF96\n"\
        "ZBja3xApZuxqM62F6dV4FQqzFX0WWhWp5n301N33r0qR6FumMKJzmVJ1TA8tmzEF\n"\
        "yINRAoGBAJqioYs8rK6eXzA8ywYLjqTLu/yQSLBn/4ta36K8DyCoLNlNxSuox+A5\n"\
        "w6z2vEfRVQDq4Hm4vBzjdi3QfYLNkTiTqLcvgWZ+eX44ogXtdTDO7c+GeMKWz4XX\n"\
        "uJSUVL5+CVjKLjZEJ6Qc2WZLl94xSwL71E41H4YciVnSCQxVc4Jw\n"\
        "-----END RSA PRIVATE KEY-----\n";

std::string EncryptKeys::pubKey = "-----BEGIN PUBLIC KEY-----\n"\
        "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAy8Dbv8prpJ/0kKhlGeJY\n"\
        "ozo2t60EG8L0561g13R29LvMR5hyvGZlGJpmn65+A4xHXInJYiPuKzrKUnApeLZ+\n"\
        "vw1HocOAZtWK0z3r26uA8kQYOKX9Qt/DbCdvsF9wF8gRK0ptx9M6R13NvBxvVQAp\n"\
        "fc9jB9nTzphOgM4JiEYvlV8FLhg9yZovMYd6Wwf3aoXK891VQxTr/kQYoq1Yp+68\n"\
        "i6T4nNq7NWC+UNVjQHxNQMQMzU6lWCX8zyg3yH88OAQkUXIXKfQ+NkvYQ1cxaMoV\n"\
        "PpY72+eVthKzpMeyHkBn7ciumk5qgLTEJAfWZpe4f4eFZj/Rc8Y8Jj2IS5kVPjUy\n"\
        "wQIDAQAB\n"\
        "-----END PUBLIC KEY-----\n";
