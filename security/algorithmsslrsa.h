#ifndef ALGORITHMSSLRSA_H
#define ALGORITHMSSLRSA_H

#include <string>
#include <openssl/rsa.h>

#include "asymmetricalgorithm.h"

class AlgorithmSslRSA: public AsymmetricAlgorithm
{
public:
    AlgorithmSslRSA(std::string const&, std::string const&);
    ~AlgorithmSslRSA();

    void encrypt(std::string& message);
    void decrypt(std::string& message);

private:
    RSA* loadPrivateKeyFromString(char const* key);
    RSA* loadPublicKeyFromString(char const* key);

private:
    std::string m_pubKey;
    std::string m_privKey;
};

#endif // ALGORITHMSSLRSA_H
