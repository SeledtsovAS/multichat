#ifndef ABSTRACTFACTORY_H
#define ABSTRACTFACTORY_H

class SymmetricAlgorithm;
class AsymmetricAlgorithm;

class AbstractFactory
{
public:
    virtual ~AbstractFactory() = 0;

    virtual SymmetricAlgorithm* getSymmetricAlgorithm() const = 0;
    virtual AsymmetricAlgorithm* getAsymmetricAlgorithm() const = 0;
};

#endif // ABSTRACTFACTORY_H
