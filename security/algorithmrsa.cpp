#include <crypto++/base64.h>
#include <crypto++/osrng.h>
#include <crypto++/des.h>
#include <crypto++/modes.h>
#include <string>

#include "algorithmrsa.h"

#define unused(var) static_cast<void>(var);

AlgorithmRSA::AlgorithmRSA(std::string const& pubKey, std::string const& privKey)
{
    CryptoPP::StringSource publicKey(pubKey, true, new CryptoPP::Base64Decoder);
    CryptoPP::StringSource privateKey(privKey, true, new CryptoPP::Base64Decoder);

    m_decryptor = CryptoPP::RSAES_OAEP_SHA_Decryptor(privateKey);
    m_encryptor = CryptoPP::RSAES_OAEP_SHA_Encryptor(publicKey);
}

AlgorithmRSA::~AlgorithmRSA() {

}

void AlgorithmRSA::encrypt(std::string& message)
{
    std::string result = "";
    CryptoPP::AutoSeededRandomPool rand;
    CryptoPP::StringSource transform(message, true, new CryptoPP::PK_EncryptorFilter(rand, m_encryptor,
                                         new CryptoPP::Base64Encoder(new CryptoPP::StringSink(result))));
    unused(transform);
    message = result;
}

void AlgorithmRSA::decrypt(std::string& message)
{
    std::string result = "";
    CryptoPP::AutoSeededRandomPool rand;
    CryptoPP::StringSource transform(message, true, new CryptoPP::Base64Decoder(new CryptoPP::PK_DecryptorFilter(
                                                            rand, m_decryptor, new CryptoPP::StringSink(result))));
    unused(transform);
    message = result;
}
