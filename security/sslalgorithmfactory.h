#ifndef SSLALGORITHMFACTORY_H
#define SSLALGORITHMFACTORY_H

#include "abstractfactory.h"

class AsymmetricAlgorithm;
class SymmetricAlgorithm;

class SSLAlgorithmFactory: public AbstractFactory
{
public:
    ~SSLAlgorithmFactory();

    AsymmetricAlgorithm* getAsymmetricAlgorithm() const;
    SymmetricAlgorithm* getSymmetricAlgorithm() const;
};

#endif // SSLALGORITHMFACTORY_H
