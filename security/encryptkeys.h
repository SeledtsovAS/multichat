#ifndef ENCRYPTKEYS_H
#define ENCRYPTKEYS_H

#include <string>

class EncryptKeys
{
public:
    static std::string privateKey;
    static std::string publicKey;
    static std::string pubKey;
    static std::string privKey;
};

#endif // ENCRYPTKEYS_H
