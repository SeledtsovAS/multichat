#include <openssl/des.h>
#include <cstring>

#include "algorithmssldes.h"

AlgorithmSslDES::~AlgorithmSslDES() {

}

void AlgorithmSslDES::decrypt(const std::string &key, std::string &message)
{
    int size = message.size();
    char* result = new char[size];
    int n = 0;
    DES_cblock key2;
    DES_key_schedule schedule;

    std::memcpy(key2, key.c_str(), 8);
    DES_set_odd_parity(&key2);
    DES_set_key_checked(&key2, &schedule);

    DES_cfb64_encrypt(reinterpret_cast<const unsigned char*>(message.c_str()),
        reinterpret_cast<unsigned char*>(result), size, &schedule, &key2, &n, DES_DECRYPT);
    message = std::string(result, result + size);

    delete [] result;
}

void AlgorithmSslDES::encrypt(std::string &key, std::string &message)
{
    int size = message.size();
    char* result = new char[size];
    int n = 0;
    DES_cblock key2;
    DES_key_schedule schedule;

    key = "password";

    std::memcpy(key2, key.c_str(), 8);
    DES_set_odd_parity(&key2);
    DES_set_key_checked(&key2, &schedule);

    DES_cfb64_encrypt(reinterpret_cast<const unsigned char*>(message.c_str()),
        reinterpret_cast<unsigned char*>(result), size, &schedule, &key2, &n, DES_ENCRYPT);
    message = std::string(result, result + size);

    delete [] result;
}
