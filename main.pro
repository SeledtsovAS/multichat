QT += widgets gui core network

SOURCES += \
    clientchat.cpp \
    tcpserver.cpp \
    protocol/protocol.pb.cc \
    io/baseiomanager.cpp \
    io/iomanager.cpp \
    io/baseiodecorator.cpp \
    io/criptodecoratorio.cpp \
    logger/baselogger.cpp \
    logger/clientlogger.cpp \
    logger/implementlog.cpp \
    logger/filelog.cpp \
    logger/serverlogger.cpp \
    logger/singletonlogger.cpp \
    security/cryptoppsystem.cpp \
    security/symmetricalgorithm.cpp \
    security/asymmetricalgorithm.cpp \
    security/algorithmrsa.cpp \
    security/algorithmdes.cpp \
    security/encryptkeys.cpp \
    security/algorithmsslrsa.cpp \
    security/algorithmssldes.cpp \
    exceptoins/baseexception.cpp \
    exceptoins/loggerexception.cpp \
    messages/basemessage.cpp \
    messages/message.cpp \
    exceptoins/messageexception.cpp \
    protocol/protocolstrategy.cpp \
    protocol/protocolimpl.cpp \
    protocol/bufferprotocolimpl.cpp \
    messages/messageonline.cpp \
    messages/messageansweronline.cpp \
    io/decoratorfilter.cpp \
    messages/messagegetaccess.cpp \
    messages/messageaccess.cpp \
    messages/messagefile.cpp \
    protocol/textprotocolimpl.cpp \
    security/abstractfactory.cpp \
    security/cryptoppalgorithmfactory.cpp \
    security/sslalgorithmfactory.cpp \
    main.cpp


QMAKE_CXXFLAGS += -std=c++11 -pthread -lcrypto -lssl
QMAKE_LFLAGS += -pthread -lprotobuf -lcrypto++ -lcrypto -lssl
LIBS += -L/usr/local/lib -I/usr/local/include -lprotobuf -lcrypto++ -lcrypto -lssl

INCLUDEPATH += ./include ./src

HEADERS += \
    clientchat.h \
    tcpserver.h \
    protocol/protocol.pb.h \
    io/baseiomanager.h \
    io/iomanager.h \
    io/baseiodecorator.h \
    io/criptodecoratorio.h \
    logger/baselogger.h \
    logger/clientlogger.h \
    logger/implementlog.h \
    logger/filelog.h \
    logger/serverlogger.h \
    logger/singletonlogger.h \
    security/cryptoppsystem.h \
    security/symmetricalgorithm.h \
    security/asymmetricalgorithm.h \
    security/algorithmrsa.h \
    security/algorithmdes.h \
    security/encryptkeys.h \
    security/algorithmsslrsa.h \
    security/algorithmssldes.h \
    exceptoins/baseexception.h \
    exceptoins/loggerexception.h \
    messages/basemessage.h \
    messages/message.h \
    exceptoins/messageexception.h \
    protocol/protocolstrategy.h \
    protocol/protocolimpl.h \
    protocol/bufferprotocolimpl.h \
    messages/messageonline.h \
    messages/messageansweronline.h \
    io/decoratorfilter.h \
    messages/messagegetaccess.h \
    messages/messageaccess.h \
    messages/messagefile.h \
    protocol/textprotocolimpl.h \
    security/abstractfactory.h \
    security/cryptoppalgorithmfactory.h \
    security/sslalgorithmfactory.h

FORMS += \
    mainwindow.ui

OTHER_FILES += \
    protocol/protocol.proto \
    protocol/install.sh
