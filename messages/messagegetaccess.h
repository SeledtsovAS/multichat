#ifndef MESSAGEGETACCESS_H
#define MESSAGEGETACCESS_H

#include <string>

#include "basemessage.h"

class MessageGetAccess: public BaseMessage
{
public:
    MessageGetAccess(int const from, int const to, int const idObj);
    ~MessageGetAccess();

    std::string getTitle() const;

    void setTitle(std::string const&);

private:
    std::string m_title;
};

#endif // MESSAGEGETACCESS_H
