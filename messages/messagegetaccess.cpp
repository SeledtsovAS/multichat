#include <QString>

#include "messagegetaccess.h"

MessageGetAccess::MessageGetAccess(int const from, int const to, int const idObj):
    BaseMessage(from, to, TypeMessage::GET_ACCESS) {

    m_title = QString::number(idObj).toStdString();
}

MessageGetAccess::~MessageGetAccess() {

}

void MessageGetAccess::setTitle(const std::string &objId)
{
    m_title = objId;
}

std::string MessageGetAccess::getTitle() const
{
    return m_title;
}
