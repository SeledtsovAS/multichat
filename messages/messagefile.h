#ifndef MESSAGEFILE_H
#define MESSAGEFILE_H

#include "basemessage.h"

class MessageFile: public BaseMessage
{
public:
    MessageFile(int const from, int const to, std::string const& namefile);
    ~MessageFile();

    void setTitle(std::string const&);
    void setBody(std::string const&);

    std::string getTitle() const;
    std::string getBody() const;

    void load();

private:
    std::string m_namefile;
    std::string m_body;
};

#endif // MESSAGEFILE_H
