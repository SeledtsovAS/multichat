#ifndef MESSAGE_H
#define MESSAGE_H

#include <string>
#include <vector>

#include "basemessage.h"

class Message: public BaseMessage
{
public:
    Message(int const from, int const to);
    ~Message();

    void setBody(std::string const& body);

    std::string getBody() const;

private:
    std::string m_body;
};

#endif // MESSAGE_H
