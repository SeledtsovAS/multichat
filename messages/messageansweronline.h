#ifndef MESSAGEANSWERONLINE_H
#define MESSAGEANSWERONLINE_H

#include <string>

#include "basemessage.h"

class MessageAnswerOnline: public BaseMessage
{
public:
    MessageAnswerOnline(int const from, int const to);
    ~MessageAnswerOnline();
};

#endif // MESSAGEANSWERONLINE_H
