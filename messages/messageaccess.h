#ifndef MESSAGEACCESS_H
#define MESSAGEACCESS_H

#include <string>

#include "basemessage.h"

class MessageAccess: public BaseMessage
{
public:
    MessageAccess(int const from, int const to, std::string const& answ);
    ~MessageAccess();

    std::string getBody() const;
    std::string getTitle() const;

    void setBody(std::string const&);
    void setTitle(std::string const&);

private:
    std::string m_answ;
    std::string m_objId;
};

#endif // MESSAGEACCESS_H
