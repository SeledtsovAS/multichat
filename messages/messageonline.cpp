#include <QString>
#include <string>
#include <vector>

#include "messageonline.h"
#include "exceptoins/messageexception.h"

MessageOnline::MessageOnline(int const from, int const to):
    BaseMessage(from, to, BaseMessage::TypeMessage::LIST_ONLINE)
{
    m_body = "";
}

MessageOnline::MessageOnline(int const from, int const to, std::vector<int> const& vec):
    BaseMessage(from, to, BaseMessage::TypeMessage::LIST_ONLINE)
{
    std::string result;
    for (int i = 0; i < vec.size(); i++)
        result += QString::number(vec[i]).toStdString() + (vec.size() - 1 == i ? "" : " ");

    m_body = result;
}

MessageOnline::~MessageOnline() {

}

std::string MessageOnline::getBody() const
{
    return m_body;
}

void MessageOnline::setBody(std::string const& message)
{
    m_body = message;
}
