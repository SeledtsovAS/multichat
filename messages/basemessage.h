#ifndef BASEMESSAGE_H
#define BASEMESSAGE_H

#include <string>
#include <vector>

class BaseMessage
{
public:
    enum class TypeMessage: char
    {
        SMS = 0x001,
        FILE = 0x002,
        GET_LIST_ONLINE = 0x003,
        LIST_ONLINE = 0x004,
        ACCESS = 0x005,
        GET_ACCESS = 0x006
    };

public:
    BaseMessage(int const from, int const to, TypeMessage const type);
    virtual ~BaseMessage();

    virtual TypeMessage getType() const;
    virtual std::string getTitle() const;
    virtual std::string getBody() const;
    virtual std::string getKey() const;
    virtual int getFrom() const;
    virtual int getTo() const;

    virtual void setTitle(std::string const& title);
    virtual void setBody(std::string const& body);
    virtual void setType(TypeMessage const type);
    virtual void setFrom(int const from);
    virtual void setTo(int const to);
    virtual void setKey(std::string const& key);

private:
    int m_to;
    int m_from;
    TypeMessage m_type;
    std::string m_key;
};

#endif // BASEMESSAGE_H
