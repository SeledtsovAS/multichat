#include "basemessage.h"

#include "exceptoins/messageexception.h"

BaseMessage::BaseMessage(int const from, int const to, TypeMessage const type) {
    m_to = to;
    m_from = from;
    m_type = type;
    m_key = "";
}

BaseMessage::~BaseMessage() {

}

int BaseMessage::getTo() const {
    return m_to;
}

int BaseMessage::getFrom() const {
    return m_from;
}

BaseMessage::TypeMessage BaseMessage::getType() const {
    return m_type;
}

void BaseMessage::setTo(int const to) {
    m_to = to;
}

void BaseMessage::setFrom(int const from) {
    m_from = from;
}

void BaseMessage::setType(TypeMessage const type) {
    m_type = type;
}

void BaseMessage::setKey(const std::string &key) {
    m_key = key;
}

std::string BaseMessage::getKey() const {
    return m_key;
}

std::string BaseMessage::getBody() const {
    return "dialog";//throw MessageNotFoundMethod("BaseMessage::getBody()");
}

std::string BaseMessage::getTitle() const {
    throw MessageNotFoundMethod("BaseMessage::getTitle()");
}

void BaseMessage::setTitle(std::string const &) {
    throw MessageNotFoundMethod("BaseMessage::setTitle()");
}

void BaseMessage::setBody(std::string const &) {
    throw MessageNotFoundMethod("BaseMessage::setBody()");
}
