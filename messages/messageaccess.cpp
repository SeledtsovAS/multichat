#include "messageaccess.h"

MessageAccess::MessageAccess(int const from, int const to, std::string const& answ):
    BaseMessage(from, to, TypeMessage::ACCESS) {

    m_answ = answ;
}

MessageAccess::~MessageAccess() {

}

void MessageAccess::setBody(std::string const& answ)
{
    m_answ = answ;
}

void MessageAccess::setTitle(std::string const& objId)
{
    m_objId = objId;
}

std::string MessageAccess::getBody() const
{
    return m_answ;
}

std::string MessageAccess::getTitle() const
{
    return m_objId;
}

