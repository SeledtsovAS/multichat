#include <string>
#include <vector>

#include "message.h"
#include "exceptoins/messageexception.h"

Message::Message(int const from, int const to):
    BaseMessage(from, to, TypeMessage::SMS) {

}

Message::~Message() {

}

void Message::setBody(std::string const& body) {
    m_body = body;
}

std::string Message::getBody() const {
    return m_body;
}
