#ifndef MESSAGEONLINE_H
#define MESSAGEONLINE_H

#include <vector>

#include "basemessage.h"

class MessageOnline: public BaseMessage
{
public:
    MessageOnline(int const from, int const to, std::vector<int> const& vec);
    MessageOnline(int const from, int const to);
    ~MessageOnline();

    void setBody(std::string const&);

    std::string getBody() const;

private:
    std::string m_body;
};

#endif // MESSAGEONLINE_H
