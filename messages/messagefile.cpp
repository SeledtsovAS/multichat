#include <fstream>

#include "messagefile.h"

MessageFile::MessageFile(int const from, int const to, std::string const&file):
    BaseMessage(from, to, TypeMessage::FILE) {

    m_namefile = file;
}

MessageFile::~MessageFile() {

}

void MessageFile::setTitle(std::string const& name) {
    m_namefile = name;
}

void MessageFile::setBody(std::string const& body) {
    m_body = body;
}

std::string MessageFile::getTitle() const
{
    return m_namefile;
}

std::string MessageFile::getBody() const
{
    return m_body;
}

void MessageFile::load()
{
    std::ifstream file(m_namefile);
    if (file.is_open()) {
        while (file) {
            char buffer[1024] {};
            file.read(buffer, 1024);
            m_body += buffer;
        }
        file.close();
    }
}
