#include "messageansweronline.h"

MessageAnswerOnline::MessageAnswerOnline(int const from, int const to):
    BaseMessage(from, to, BaseMessage::TypeMessage::GET_LIST_ONLINE) {

}

MessageAnswerOnline::~MessageAnswerOnline() {

}
