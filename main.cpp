#include <QApplication>

#include "tcpserver.h"
#include "logger/serverlogger.h"
#include "logger/singletonlogger.h"
#include "clientchat.h"

#define unused(val) static_cast<void>(val);

int main(int argc, char** argv)
{
    QApplication app(argc, argv);
   // TCPServer server(1888, SingletonLogger<ServerLogger>::getInstance());
    //unused(server);
    ClientChat client;
    client.show();

    return app.exec();
}
