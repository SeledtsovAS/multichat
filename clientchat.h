#ifndef CLIENTCHAT_H
#define CLIENTCHAT_H

#include <QMainWindow>
#include <QSharedPointer>
#include <QAbstractSocket>
#include <messages/basemessage.h>
#include <functional>
#include <map>

#include "ui_mainwindow.h"

class QTcpSocket;
class BaseIOManager;
class BaseLogger;

class ClientChat: public QMainWindow, private Ui::MainWindow
{
    Q_OBJECT

public:
    ClientChat(QMainWindow* parent = 0);
    ~ClientChat();

public slots:
    void slotReadyRead();
    void slotSendToServer(BaseMessage&);
    void slotConnected();
    void slotError(QAbstractSocket::SocketError);
    void slotDisconnect();

private slots:
    void on_btnConnect_2_clicked();
    void on_btnDisconnect_2_clicked();
    void on_btnSend_2_clicked();
    void on_btnClientsUpdate_2_clicked();
    void on_btnSendFile_2_clicked();

private:
    void updateTxtLog();
    void handlerMessageOnline(QSharedPointer<BaseMessage>);
    void handlerMessageGetAccess(QSharedPointer<BaseMessage>);
    void handlerMessageAccess(QSharedPointer<BaseMessage>);
    void handlerMessageFile(QSharedPointer<BaseMessage>);
    void handlerMessageSMS(QSharedPointer<BaseMessage>);

private:
    QSharedPointer<QTcpSocket> m_socket;
    QSharedPointer<BaseIOManager> m_ioManager;
    QSharedPointer<BaseLogger> m_logger;
    std::map<BaseMessage::TypeMessage, std::function<void(ClientChat&, QSharedPointer<BaseMessage>)>> handlerMapper;
    bool m_isConnection;
};

#endif // CLIENTCHAT_H
