#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <QWidget>
#include <QSharedPointer>
#include <functional>
#include <map>

#include "messages/basemessage.h"

class BaseLogger;
class QTcpServer;
class QTcpSocket;
class BaseIOManager;
class QString;

class TCPServer: public QWidget
{
    Q_OBJECT

public:
    TCPServer(const int port, BaseLogger* logger, QWidget* parent = 0);
    virtual ~TCPServer();

private:
    void sendToClient(QTcpSocket* sock, BaseMessage& message);
    std::vector<int> getVectorClients(QTcpSocket* socket);
    int getId(QTcpSocket* socket);

    void handlerMessageOnline(QSharedPointer<BaseMessage>, QTcpSocket*);
    void handlerMessageSmsOrFile(QSharedPointer<BaseMessage>, QTcpSocket*);
    void handlerMessageAccess(QSharedPointer<BaseMessage>, QTcpSocket*);

public slots:
    virtual void slotNewConnection();
    void slotClientRead();
    void slotDisconnect();

private:
    QSharedPointer<QTcpServer> m_server;
    quint16 m_nextBlockSize;
    QSharedPointer<BaseLogger> m_logger;
    QSharedPointer<BaseIOManager> m_ioManager;
    std::map<int, QTcpSocket*> m_clients;
    static unsigned int m_nClients;

    static unsigned int m_nObjects;
    std::map<int, QSharedPointer<BaseMessage>> m_objects;
    std::map<BaseMessage::TypeMessage, std::function<void(TCPServer&, QSharedPointer<BaseMessage>, QTcpSocket*)>> handlerMapper;
};

#endif // TCPSERVER_H
